import { Component, FormBaseInput, Params } from '@orourley/elm-app';

export class FormEntry extends Component {

	/** Constructs the component. */
	constructor(params: Params) {
		super(params);

		// Insert the inner component html.
		this.insertHtml(params.innerHtml, this.query(':scope > .inputArea > .inputComponent', Element), undefined, params.innerHtmlContext);
		if (!this.queryHasComponent(':scope > .inputArea > .inputComponent > :first-child', Component)) {
			throw new Error(`The first element of the inner html of a FormEntry must be a Component, but found ${this.query(':scope > .inputArea > .inputComponent', Element).innerHTML}.`);
		}

		// Get the input component.
		this._inputComponent = this.queryComponent(':scope > .inputArea > .inputComponent > :first-child', Component) as Component;

		// Set the name, if any.
		const name = params.attributes.get('name');
		if (name !== undefined) {
			if (!(this._inputComponent instanceof FormBaseInput)) {
				throw new Error('If a name is given, the first element of the inner html of the FormEntry must be a FormBaseInput.');
			}
			this.root.classList.add(name);
			this.query(':scope > .inputArea > .labelLine > label', HTMLLabelElement).htmlFor = name;
			this._inputComponent.name = name;
			this._inputComponent.id = name;
		}

		// Set the label, if any.
		const label = params.attributes.get('label');
		if (label !== undefined) {
			this.query(':scope > .inputArea > .labelLine > label', Element).innerHTML = label;
		}

		// If there's a tip, move it to the tip section and hide it.
		if (this.queryHas(':scope > .inputArea > .inputComponent > .tip')) {
			const tip = this.query(':scope > .inputArea > .inputComponent > .tip', Element);
			tip.classList.add('hidden');
			this.root.append(tip);
		}
		// Otherwise, remove the question mark and tip.
		else {
			this.query(':scope > .inputArea > .labelLine > .toggleTip', Element).remove();
		}
	}

	/** Gets the root class list. */
	get classList(): DOMTokenList {
		return this.root.classList;
	}

	/** Gets the input. */
	getInput<Type extends Component>(type: { new (params: Params): Type; }): Type {
		if (!(this._inputComponent instanceof type)) {
			throw new ReferenceError(`The component of type ${this._inputComponent?.constructor.name} is not of type ${type.name}.`);
		}
		return this._inputComponent;
	}

	/** Toggles the tip. */
	protected toggleTip(): void {
		this.query(':scope > .tip', Element).classList.toggle('hidden');
	}

	/** The input component. */
	private _inputComponent: Component;

	static override html = `
		<div class="uniqueIds">
			<div class="inputArea">
				<div class="labelLine">
					<label></label>
					<a class="toggleTip" href="javascript:;" onclick="toggleTip">Tip</a>
				</div>
				<div class="inputComponent"></div>
			</div>
			<div class="invalid">! Please enter a valid value.</div>
		</div>
		`;

	static override css = `
		.FormEntry {
			display: flex;
			flex-direction: column;
			gap: .25rem;
			max-width: 100%;
		}
		.FormEntry > .inputArea > .labelLine {
			display: flex;
			flex-direction: row;
			gap: .5rem;
		}
		.FormEntry > .inputArea > .labelLine > .toggleTip {
			margin-inline-start: auto;
			opacity: 70%;
		}
		.FormEntry > .tip {
			border: 1px solid currentColor;
			border-radius: .25rem;
			padding: .125rem .25rem;
		}
		.FormEntry > .invalid {
			display: none;
		}
		.FormEntry > .inputArea:has(.invalid) + .invalid {
			display: block;
			content: '! Please enter a valid value.';
		}
		.FormEntry > .inputArea > .inputComponent .FormInput {
			width: 100%;
		}
		`;
}
