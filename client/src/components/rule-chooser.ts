import { Params } from '@orourley/elm-app';
import { ItemChooser } from './item-chooser';

export class RuleChooser extends ItemChooser {

	/** Constructor. */
	constructor(params: Params) {
		super('Rule', 'rules', params);
	}
}
