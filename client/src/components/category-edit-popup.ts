import { FormEasy, FormValues, Params } from '@orourley/elm-app';
import { Items } from '@orourley/financio-types';
import { ItemEditPopup } from './item-edit-popup';

export class CategoryEditPopup extends ItemEditPopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Category', 'categories', params);
	}

	/** Gets any other form entries to put in after the base item elements. */
	protected getExtraFormEntriesHtml(): string {
		return ``;
	}

	/** Populates the form with any default values when we're creating. */
	protected async populateExtraFieldDefaults(_form: FormEasy): Promise<void> {
	}

	/** Populates the form with any extra values. */
	protected async populateExtraFieldValues(_id: string, _item: Items.Item, _form: FormEasy): Promise<void> {
	}

	/** Gets any extra form values for sending to the server. */
	protected getExtraFormValues(_values: FormValues): Record<string, any> {
		return {};
	}
}
