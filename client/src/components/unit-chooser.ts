import { Params } from '@orourley/elm-app';
import { ItemChooser } from './item-chooser';

export class UnitChooser extends ItemChooser {

	/** Constructor. */
	constructor(params: Params) {
		super('Unit', 'units', params);
	}
}
