import { Component, Params, Sanitizer, FormHelper, DropDown } from '@orourley/elm-app';
import { AccountFile, CategoryFile, Items, Transaction, UnitFile } from '@orourley/financio-types';

import { JoinPopup } from './join-popup';

import html from './transaction-view.html';
import css from './transaction-view.css';
import { FinancioApp } from '../financio-app';
import { SplitPopup } from './split-popup';

export class TransactionView extends Component {

	onSaved: ((transactionView: TransactionView) => void) | undefined;
	onJoined: ((updatedTransactionid: string, deletedTransactionId: string) => void) | undefined;
	onSplit: ((newTransactions: Transaction[], deletedTransactionId: string) => void) | undefined;
	onDeleted: ((transactionView: TransactionView) => void) | undefined;
	onNeedsDuplication: ((transactionView: TransactionView) => void) | undefined;

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Save the attributes.
		this._editable = params.attributes.has('editable');
		this._new = params.attributes.has('new');
		if (this._new && !this._editable) {
			throw new Error('Cannot have an un-editable new transaction.');
		}

		// Get the event handlers.
		this.onSaved = params.eventHandlers.get('saved');
		this.onDeleted = params.eventHandlers.get('deleted');
		this.onJoined = params.eventHandlers.get('joined');
		this.onSplit = params.eventHandlers.get('split');
		this.onNeedsDuplication = params.eventHandlers.get('needsduplication');

		// If new, immediately start the edit mode.
		if (this._new) {
			this._edit = true;
		}
	}

	/** Destroys the FormSelect. */
	protected override destroy(): void {
		if (this._buttonsDropDown) {
			this.app.closeOverlay(this._buttonsDropDown);
		}
	}

	/** Gets the transaction. */
	getTransaction(): Transaction | undefined {
		return this._transaction;
	}

	/** Sets the transaction. */
	setTransaction(transaction: Transaction | undefined, files: { accountFile: AccountFile; categoryFile: CategoryFile; unitFile: UnitFile; }): void {

		// Save the parameters.
		this._transaction = transaction;
		this._files = files;

		// Do edit mode if there is no transaction or it is unreviewed.
		// this._edit = !this._transaction || !this._transaction.reviewed;

		// Update the view.
		this.updateView();
	}

	/** Makes the whole transaction clickable. */
	setClickable(clickEventListener: (() => void) | undefined): void {
		if (this._clickEventListener) {
			this.root.removeEventListener('click', this._clickEventListener);
			this.root.classList.remove('clickable');
		}
		this._clickEventListener = clickEventListener;
		if (this._clickEventListener) {
			this.root.addEventListener('click', this._clickEventListener);
			this.root.classList.add('clickable');
		}
	}

	/** Updates the view with the current data. */
	private updateView(): void {

		// Clear everything first.
		this.clear();

		// If we don't have the required variable, don't show anything.
		if (!this._files) {
			return;
		}

		// Get the accountFrom, accountTo, and category.
		let accountFrom = this._files.accountFile.list[this._transaction?.accountIdFrom ?? ''];
		if (accountFrom && Items.isGroup(accountFrom)) {
			accountFrom = undefined;
		}
		let accountTo = this._files.accountFile.list[this._transaction?.accountIdTo ?? ''];
		if (accountTo && Items.isGroup(accountTo)) {
			accountTo = undefined;
		}
		let category = this._files.categoryFile.list[this._transaction?.categoryId ?? ''];
		if (category && Items.isGroup(category)) {
			category = undefined;
		}

		// Get the unit from and to.
		let unitFrom;
		let unitTo;
		if (accountFrom) {
			unitFrom = this._files.unitFile.list[accountFrom.unitId];
			if (unitFrom && Items.isGroup(unitFrom)) {
				unitFrom = undefined;
			}
		}
		if (accountTo) {
			unitTo = this._files.unitFile.list[accountTo.unitId];
			if (unitTo && Items.isGroup(unitTo)) {
				unitTo = undefined;
			}
		}

		// Set the id attribute for the root.
		if (this._transaction) {
			this.root.setAttribute('transactionId', this._transaction.id);
		}

		// Set the edit class.
		this.root.classList.toggle('edit', this._edit);

		// Set all of the fields.
		if (this._edit) {
			this.setHtml(`<AccountChooser name="accountFrom" onChosen="accountChosen|from" showAdd showBlank value="${accountFrom?.id ?? ''}"></AccountChooser>`, this.query('.accountFrom', Element), this);
			this.setHtml(`<AccountChooser name="accountTo" onChosen="accountChosen|to" showAdd showBlank value="${accountTo?.id ?? ''}"></AccountChooser>`, this.query('.accountTo', Element), this);
			this.setHtml(`<DateChooser name="dateFrom" value="${this._transaction?.dateFrom ?? ''}"></DateChooser>`, this.query('.dateFrom', Element), this);
			this.setHtml(`<DateChooser name="dateTo" value="${this._transaction?.dateFrom ?? ''}"></DateChooser>`, this.query('.dateTo', Element), this);
			this.setHtml(`<FormNumberInput name="amountFrom" value="${this._transaction?.amountFrom ?? ''}" decimals="${unitFrom?.decimals ?? ''}"></FormNumberInput>`, this.query('.amountFrom', Element), this);
			this.setHtml(`<FormNumberInput name="amountTo" value="${this._transaction?.amountTo ?? ''}" decimals="${unitTo?.decimals ?? ''}"></FormNumberInput>`, this.query('.amountTo', Element), this);
			this.setHtml(`<CategoryChooser name="category" onChosen="categoryChosen" showAdd showBlank value="${this._transaction?.categoryId ?? ''}"></CategoryChooser>`, this.query('.category', Element), this);
			this.setHtml(`<FormCheckbox name="reviewed" value="${this._transaction?.reviewed === true ? 'true' : 'false'}"></FormCheckbox>`, this.query('.reviewed', Element), this);
			this.setHtml(Sanitizer.sanitizeForHtml(this._transaction?.description ?? ''), this.query('.description', Element), this);
			this.setHtml(`<FormInput name="note" value="${Sanitizer.sanitizeForAttribute(this._transaction?.note ?? '')}"></FormInput>`, this.query('.note', Element), this);

			this.setHtml(`
				<button class="saveButton button icon" onclick="saveEdit">✔</button>
				<button class="cancelButton button icon" onclick="cancelEdit">X</button>
				`, this.query('.buttons', Element), this);
		}
		else {
			this.setHtml(Sanitizer.sanitizeForHtml(accountFrom?.name ?? ''), this.query('.accountFrom', Element), this);
			this.setHtml(Sanitizer.sanitizeForHtml(accountTo?.name ?? ''), this.query('.accountTo', Element), this);
			this.setHtml(this._transaction?.dateFrom ?? '', this.query('.dateFrom', Element), this);
			this.setHtml(this._transaction?.dateTo ?? '', this.query('.dateTo', Element), this);
			this.setHtml((unitFrom ? this._transaction?.amountFrom.toFixed(unitFrom.decimals) : this._transaction?.amountFrom.toString()) ?? '', this.query('.amountFrom', Element), this);
			this.setHtml((unitTo ? this._transaction?.amountTo.toFixed(unitTo.decimals) : this._transaction?.amountTo.toString()) ?? '', this.query('.amountTo', Element), this);
			this.setHtml(Sanitizer.sanitizeForHtml(category?.name ?? ''), this.query('.category', Element), this);
			this.setHtml(this._transaction?.reviewed === true ? '✔' : '', this.query('.reviewed', Element), this);
			this.setHtml(Sanitizer.sanitizeForHtml(this._transaction?.description ?? ''), this.query('.description', Element), this);
			this.setHtml(Sanitizer.sanitizeForHtml(this._transaction?.note ?? ''), this.query('.note', Element), this);

			if (this._editable) {
				this.setHtml(`
					<button class="editButton button icon" onclick="showEdit"><icon src="assets/icons/edit.svg" alt="edit"></icon></button>
					<button class="button icon" onclick="showButtonsDropDown"><icon src="assets/icons/menu.svg" alt="menu"></icon></button>
					`, this.query('.buttons', Element), this);
			}
		}
		this.setHtml(Sanitizer.sanitizeForHtml(unitFrom?.name ?? ''), this.query('.unitFrom', Element), this);
		this.setHtml(Sanitizer.sanitizeForHtml(unitTo?.name ?? ''), this.query('.unitTo', Element), this);
	}

	private clear(): void {
		this.clearNode(this.query('.accountFrom', Element));
		this.clearNode(this.query('.accountTo', Element));
		this.clearNode(this.query('.dateFrom', Element));
		this.clearNode(this.query('.dateTo', Element));
		this.clearNode(this.query('.amountFrom', Element));
		this.clearNode(this.query('.amountTo', Element));
		this.clearNode(this.query('.unitFrom', Element));
		this.clearNode(this.query('.unitTo', Element));
		this.clearNode(this.query('.category', Element));
		this.clearNode(this.query('.reviewed', Element));
		this.clearNode(this.query('.description', Element));
	}

	/** When the buttons drop down is pressed. */
	protected showButtonsDropDown(): void {

		// First close any existing drop down.
		if (this._buttonsDropDown) {
			this._buttonsDropDown.close();
		}

		// Open the drop down.
		this._buttonsDropDown = this.app.openOverlay<DropDown>(`
			<DropDown class="ButtonsDropDown input">
				<button class="joinButton button" onclick="showJoinPopup">Join</button>
				<button class="splitButton button" onclick="showSplitPopup">Split</button>
				<button class="duplicateButton button" onclick="duplicateTransaction">Clone</button>
				<button class="deleteButton button" onclick="showDeletePopup">Delete</button>
			</DropDown>
			`, this);
		this._buttonsDropDown.setRelativeTo(this.query('.buttons button', Element));
		this._buttonsDropDown.setOnClosedHandler(this.onDropDownClosed.bind(this));
	}

	/** When the drop down is closed. */
	protected onDropDownClosed(): void {
		this._buttonsDropDown = undefined;
	}

	/** When the edit mode is shown. */
	protected showEdit(): void {
		this._buttonsDropDown?.close();
		this._edit = true;
		this.updateView();
	}

	/** When the edit mode is canceled. */
	protected cancelEdit(): void {
		this._edit = false;
		this.updateView();
	}

	/** When a transaction is saved after an edit. */
	async saveEdit(): Promise<void> {

		// The transaction that will be updated.
		let transaction: Transaction;

		// Get the values.
		const values = FormHelper.getValues(this.root);
		const accountIdFrom = values['accountFrom'] as string;
		const accountIdTo = values['accountTo'] as string;
		const amountFrom = values['amountFrom'] as string;
		const amountTo = values['amountTo'] as string;
		const categoryId = values['category'] as string;
		const dateFrom = values['dateFrom'] as string;
		const dateTo = values['dateTo'] as string;
		const note = values['note'] as string;
		const reviewed = values['reviewed'] as boolean;

		try {
			// Get the amounts as numbers.
			const amountFromValue = parseFloat(amountFrom.replace(/[^0-9.+-]/gu, ''));
			if (isNaN(amountFromValue)) {
				throw new Error('The "From" amount is not a number.');
			}
			const amountToValue = parseFloat(amountTo.replace(/[^0-9.+-]/gu, ''));
			if (isNaN(amountToValue)) {
				throw new Error('The "To" amount is not a number.');
			}

			// TODO: If they have the same unit, but different amounts, error.

			// Make sure the dates have the right format.
			if (dateFrom.match(/^\d\d\d\d-\d\d-\d\d$/u) === null) {
				throw new Error('The "From" date is not in the YYYY-MM-DD format.');
			}
			if (dateTo.match(/^\d\d\d\d-\d\d-\d\d$/u) === null) {
				throw new Error('The "To" date is not in the YYYY-MM-DD format.');
			}
			if (dateFrom > dateTo) {
				throw new Error('The "From" date must be less than or equal to the "To" date.');
			}

			// Update the transaction.
			const dateFromOld = this._transaction?.dateFrom ?? dateFrom;
			transaction = {
				id: this._transaction?.id ?? '',
				accountIdFrom: accountIdFrom,
				accountIdTo: accountIdTo,
				dateFrom: dateFrom,
				dateTo: dateTo,
				categoryId: categoryId,
				amountFrom: amountFromValue,
				amountTo: amountToValue,
				description: this._transaction?.description ?? '',
				reviewed: reviewed,
				institutionIdFrom: this._transaction?.institutionIdFrom ?? '',
				institutionIdTo: this._transaction?.institutionIdTo ?? '',
				note: note
			};

			// Send the update to the server. Save the transaction id in case it was a new transaction.
			transaction.id = (await (this.app as FinancioApp).send<string[]>('transactions', 'update', {
				projectId: (this.app as FinancioApp).projectId,
				transactions: [{
					dateFromOld,
					transaction
				}]
			}))[0]!;
		}
		catch (error) {
			this.app.openOverlay(`<OkPopup><p>Error: ${Sanitizer.sanitizeForHtml((error as Error).message)}</p></OkPopup>`);
			return;
		}

		// Update the view.
		this._transaction = transaction;
		this._edit = false;
		this.updateView();
		this.onSaved?.(this);
	}

	/** When an account is selected, we update the unit. */
	protected async accountChosen(fromTo: 'from' | 'to', accountId: string, added: boolean): Promise<void> {

		if (!this._files) {
			return;
		}

		// If something was added, we should update the account and unit files.
		if (added) {
			const financioApp = this.app as FinancioApp;
			this._files.accountFile = await financioApp.send<AccountFile>('accounts', 'getFile', {
				projectId: financioApp.projectId
			});
			this._files.unitFile = await financioApp.send<UnitFile>('units', 'getFile', {
				projectId: financioApp.projectId
			});
		}

		// Get the unit name.
		let unitName = '';
		const account = this._files.accountFile.list[accountId];
		if (account && !Items.isGroup(account)) {
			const unit = this._files.unitFile.list[account.unitId];
			if (unit && !Items.isGroup(unit)) {
				unitName = unit.name;
			}
		}

		if (fromTo === 'from') {
			this.setHtml(Sanitizer.sanitizeForHtml(unitName), this.query(`.unitFrom`, Element), this);
		}
		else {
			this.setHtml(Sanitizer.sanitizeForHtml(unitName), this.query(`.unitTo`, Element), this);
		}
	}

	/** When a category is selected, we might update the file. */
	protected async categoryChosen(_categoryId: string, added: boolean): Promise<void> {

		if (!this._files) {
			return;
		}

		// If something was added, we should update the account and unit files.
		if (added) {
			const financioApp = this.app as FinancioApp;
			this._files.categoryFile = await financioApp.send<AccountFile>('categories', 'getFile', {
				projectId: financioApp.projectId
			});
		}
	}

	/** Shows the delete button popup. */
	showDeletePopup(): void {
		this._buttonsDropDown?.close();
		this.app.openOverlay(`<ConfirmPopup onYes="deletePopupYes"><p>Are you sure you want to delete this transaction?</p></ConfirmPopup>`, this);
	}

	/** When a transaction is deleted. */
	async deletePopupYes(): Promise<void> {

		// If there is no transaction, do nothing.
		if (!this._transaction) {
			return;
		}

		// Show the waiting popup.
		const waitingPopup = this.app.openOverlay(`<WaitingPopup><p>Deleting...</p></WaitingPopup>`, this);

		// Send the command.
		const financioApp = this.app as FinancioApp;
		await financioApp.send('transactions', 'delete', {
			projectId: financioApp.projectId,
			id: this._transaction.id,
			dateFrom: this._transaction.dateFrom
		});

		// Remove the waiting popup.
		this.app.closeOverlay(waitingPopup);

		// Call the callback.
		if (this.onDeleted) {
			this.onDeleted(this);
		}
	}

	/** Show the join popup. */
	async showJoinPopup(): Promise<void> {
		this._buttonsDropDown?.close();

		if (!this._transaction) {
			return;
		}

		const popup = this.app.openOverlay<JoinPopup>(`<JoinPopup onSubmitted="joinPopupSubmitted"></JoinPopup>`, this);
		popup.setTransaction(this._transaction);
	}

	joinPopupSubmitted(updatedTransactionid: string, deletedTransactionId: string): void {

		// Update the view.
		this.updateView();

		if (this.onJoined) {
			this.onJoined(updatedTransactionid, deletedTransactionId);
		}
	}

	/** Show the split popup. */
	async showSplitPopup(): Promise<void> {
		this._buttonsDropDown?.close();

		if (!this._transaction) {
			return;
		}

		const popup = this.app.openOverlay<SplitPopup>(`<SplitPopup onSubmitted="splitPopupSubmitted"></SplitPopup>`, this);
		popup.setTransaction(this._transaction);
	}

	splitPopupSubmitted(newTransactions: Transaction[]): void {

		if (!this._transaction) {
			return;
		}

		if (this.onSplit) {
			this.onSplit(newTransactions, this._transaction.id);
		}
	}

	duplicateTransaction(): void {
		this._buttonsDropDown?.close();
		if (this.onNeedsDuplication) {
			this.onNeedsDuplication(this);
		}
	}

	/** Whether or not the user can edit the transaction. */
	private _editable: boolean = true;

	/** The buttons drop down. */
	private _buttonsDropDown: DropDown | undefined;

	/** Whether or not we're in edit mode. */
	private _edit: boolean = false;

	/** Whether or not this is a new transaction. */
	private _new: boolean = false;

	/** The transaction. */
	private _transaction: Transaction | undefined;

	/** The files given by the transaction list. */
	private _files: { accountFile: AccountFile; categoryFile: CategoryFile; unitFile: UnitFile; } | undefined;

	/** If the whole transaction view is clickable, this is the event listener. */
	private _clickEventListener: (() => void) | undefined;

	protected static override html = html;
	protected static override css = css;
}
