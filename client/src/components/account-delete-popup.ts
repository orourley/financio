import { Params } from '@orourley/elm-app';
import { Account } from '@orourley/financio-types';
import { DeletePopup } from './delete-popup';

export class AccountDeletePopup extends DeletePopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Account', params);
	}

	protected override async getName(id: string): Promise<string> {

		// Get the account.
		const account = await this.app.send<Account>('accounts', 'getItem', {
			projectId: this.app.projectId,
			id: id
		});

		// Return the name.
		return account.name;
	}

	protected override async delete(id: string): Promise<void> {

		// Send the command.
		await this.app.send('accounts', 'deleteItemOrGroup', {
			projectId: this.app.projectId,
			id: id
		});
	}
}
