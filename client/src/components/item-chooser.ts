import { Params } from '@orourley/elm-app';
import { Items } from '@orourley/financio-types';
import { FinancioApp } from '../internal';
import { Chooser } from './chooser';

export abstract class ItemChooser extends Chooser {

	/** Constructor. */
	constructor(type: string, module: string, params: Params) {
		super(type, params);

		this._module = module;

		// Set the project id.
		this._projectId = params.attributes.get('projectid') ?? (this.app as FinancioApp).projectId;

		// Save some items to exclude.
		const exclude = (params.attributes.get('exclude') ?? '').split(',');
		if (exclude.length === 1 && exclude[0] === '') {
			exclude.length = 0;
		}
		this._exclude = new Set(exclude);

		// Set the only groups flag.
		const selectGroups = params.attributes.get('selectgroups');
		if (selectGroups === 'only' || selectGroups === 'also') {
			this._selectGroups = selectGroups;
		}
		if (this._selectGroups === 'only') {
			this.setType('Group');
		}
	}

	/** Add or remove an excluded item. */
	async setExcludedItem(unitId: string, exclude: boolean): Promise<void> {
		if (exclude && !this._exclude.has(unitId)) {
			this._exclude.add(unitId);
			await this.updateDisplay();
		}
		else if (!exclude && this._exclude.has(unitId)) {
			this._exclude.delete(unitId);
			await this.updateDisplay();
		}
	}

	/** Subclasses implement this which should return html for the list options. */
	protected async addOptions(): Promise<string> {

		// Get the file.
		const file = await (this.app as FinancioApp).send<Items.File>(this._module, 'list', {
			projectId: this._projectId
		});

		// Setup the html.
		let html = '';
		Items.iterateItems(file, file, (entry, depth) => {
			if (Items.isFile(entry)) {
				if (this._selectGroups !== 'none') {
					html += `<option value="" depth="${depth}" style="margin-left: ${depth * 0.5}rem;">*No group*</option>`;
				}
			}
			else if (Items.isGroup(entry)) {
				if (this._exclude.has(entry.id)) {
					return;
				}
				html += `<option${this._selectGroups !== 'none' ? ` value="${entry.id}"` : ``} depth="${depth}" style="margin-left: ${(depth - 1) * 0.5}rem;">${entry.name}</option>`;
			}
			else {
				if (this._exclude.has(entry.id) || this._selectGroups === 'only') {
					return;
				}
				html += `<option value="${entry.id}" depth="${depth}" style="margin-left: ${(depth - 1) * 0.5}rem;">${entry.name}</option>`;
			}
		});

		// Return the html.
		return html;
	}

	/** The module. */
	private _module: string;

	/** The projectId. */
	private _projectId: string | undefined;

	/** Only select only groups, items and also groups, or no group. */
	private _selectGroups: 'only' | 'also' | 'none' = 'none';

	/** A list of values to exclude. */
	private _exclude: Set<string> = new Set();
}
