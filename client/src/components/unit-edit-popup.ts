import { FormEasy, FormInput, FormValues, Params } from '@orourley/elm-app';
import { Unit } from '@orourley/financio-types';
import { ItemEditPopup } from './item-edit-popup';
import { FormEntry } from './form-entry';

import css from './unit-edit-popup.css';

export class UnitEditPopup extends ItemEditPopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Unit', 'units', params);
	}

	/** Gets any other form entries to put in after the base item elements. */
	protected getExtraFormEntriesHtml(): string {
		return `
			<FormEntry name="decimals" label="Decimals">
				<FormInput></FormInput>
				<div class="tip">
					<p>The number of decimals to always show when displaying numbers of the unit.</p>
				</div>
			</FormEntry>`;
	}

	/** Populates the form with any default values when we're creating. */
	protected async populateExtraFieldDefaults(form: FormEasy): Promise<void> {

		// Set the values.
		form.getEntries().queryComponent('.decimals', FormEntry).getInput(FormInput).value = `2`;
	}

	/** Populates the form with any extra values. */
	protected async populateExtraFieldValues(_id: string, unit: Unit, form: FormEasy): Promise<void> {

		// Set the values.
		form.getEntries().queryComponent('.decimals', FormEntry).getInput(FormInput).value = `${unit.decimals}`;
	}

	/** Gets any extra form values for sending to the server. */
	protected getExtraFormValues(values: FormValues): Record<string, any> {

		// Get the inputs.
		const decimalsAsString = values['decimals'] as string;

		// Parse the decimals as a integer.
		const decimals = Number.parseInt(decimalsAsString);
		if (isNaN(decimals)) {
			throw new Error('Please make the decimals input an integer.');
		}

		// Return the values.
		return {
			decimals
		};
	}

	protected static override css = css;
}
