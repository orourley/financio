import { Params } from '@orourley/elm-app';
import { Category } from '@orourley/financio-types';
import { DeletePopup } from './delete-popup';

export class CategoryDeletePopup extends DeletePopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Category', params);
	}

	protected override async getName(id: string): Promise<string> {

		// Get the category.
		const category = await this.app.send<Category>('categories', 'getItem', {
			projectId: this.app.projectId,
			id: id
		});

		// Return the name.
		return category.name;
	}

	protected override async delete(id: string): Promise<void> {

		// Send the command.
		await this.app.send('categories', 'deleteItemOrGroup', {
			projectId: this.app.projectId,
			id: id
		});
	}
}
