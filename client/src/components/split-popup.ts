import { Account, Transaction, Unit } from '@orourley/financio-types';
import { FormEasy, FormList, FormNumberInput, Params } from '@orourley/elm-app';
import { FinancioPopup } from '../financio-popup';

import html from './split-popup.html';
import css from './split-popup.css';
import { CategoryChooser } from './category-chooser';
import { displayAmount } from '../utils/display_amount';
import { FinancioApp } from '../financio-app';
import { MathHelper } from '@orourley/pine-lib';

export class SplitPopup extends FinancioPopup {

	/** The created callback. */
	private onSubmitted: ((newTransactions: Transaction[]) => void) | undefined;

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Get the event handlers.
		this.onSubmitted = params.eventHandlers.get('submitted');

		// Set the content of the window.
		this.setHtml(html, this.query('.window', Element), this);

		// Start off with two entries.
		const form = this.queryComponent('.FormEasy', FormEasy);
		const formList = form.getEntries().queryComponent(':scope > .entries', FormList);
		formList.add();
		formList.add();
	}

	/** Sets the transaction to be merged into. */
	async setTransaction(transaction: Transaction): Promise<void> {

		// Save the transaction.
		this._transaction = transaction;

		// Update the original amount and account.
		const accountFrom = this._transaction.accountIdFrom !== ''
			? await this.app.send<Account>('accounts', 'getItem', {
				projectId: this.app.projectId,
				id: this._transaction.accountIdFrom
			})
			: undefined;
		const accountTo = this._transaction.accountIdTo !== ''
			? await this.app.send<Account>('accounts', 'getItem', {
				projectId: this.app.projectId,
				id: this._transaction.accountIdTo
			})
			: undefined;

		// Get the units for the account from and account to.
		this._unitFrom = accountFrom && accountFrom.unitId !== ''
			? await this.app.send<Unit>('units', 'getItem', {
				projectId: this.app.projectId,
				id: accountFrom.unitId
			})
			: undefined;
		this._unitTo = accountTo && accountTo.unitId !== ''
			? await this.app.send<Unit>('units', 'getItem', {
				projectId: this.app.projectId,
				id: accountTo.unitId
			})
			: undefined;

		// If the units are the same, the amounts must match on each side.
		if (this._unitFrom && this._unitTo && this._unitFrom.id === this._unitTo.id) {
			this._amountFromToMatch = true;
		}

		// Update the precision on the form number inputs.
		const form = this.queryComponent('.FormEasy', FormEasy);
		const formList = form.getEntries().queryComponent(':scope > .entries', FormList);
		for (let entry = formList.getFirstItem(); entry; entry = formList.getNextItem(entry)) {
			entry.queryComponent('.from.FormNumberInput', FormNumberInput).decimals = this._unitFrom?.decimals;
			entry.queryComponent('.to.FormNumberInput', FormNumberInput).decimals = this._unitTo?.decimals;
		}

		// Update the original total text.
		const transactionAmountFromElem = form.getEntries().query('.transactionAmountFrom', HTMLElement);
		transactionAmountFromElem.innerHTML = displayAmount(this._transaction.amountFrom, this._unitFrom, false);
		const transactionAmountToElem = form.getEntries().query('.transactionAmountTo', HTMLElement);
		transactionAmountToElem.innerHTML = displayAmount(this._transaction.amountTo, this._unitTo, false);

		// Update the totals.
		await this.updateTotals();
	}

	/** Updates the totals for use when summing things up. */
	protected async updateTotals(fromTo?: 'from' | 'to'): Promise<void> {

		// Check that a transaction was set.
		if (!this._transaction) {
			return;
		}

		// Get the form.
		const form = this.queryComponent('.FormEasy', FormEasy);

		// Add up the amounts.
		let totalAmountFrom = 0;
		let totalAmountTo = 0;
		this._entries = [];
		const formList = form.getEntries().queryComponent(':scope > .entries', FormList);
		for (let entry = formList.getFirstItem(); entry; entry = formList.getNextItem(entry)) {
			const categoryId = entry.queryComponent('.CategoryChooser', CategoryChooser).value;

			// If the amount from and to must match (same unit), then make the other one equal this one.
			if (this._amountFromToMatch) {
				if (fromTo === 'from') {
					entry.queryComponent('.to.FormNumberInput', FormNumberInput).value = entry.queryComponent('.from.FormNumberInput', FormNumberInput).value;
				}
				else if (fromTo === 'to') {
					entry.queryComponent('.from.FormNumberInput', FormNumberInput).value = entry.queryComponent('.to.FormNumberInput', FormNumberInput).value;
				}
			}

			// Get the amount from and to.
			let amountFrom = entry.queryComponent('.from.FormNumberInput', FormNumberInput).value;
			if (typeof amountFrom !== 'number') {
				amountFrom = 0;
			}

			let amountTo = entry.queryComponent('.to.FormNumberInput', FormNumberInput).value;
			if (typeof amountTo !== 'number') {
				amountTo = 0;
			}

			// Total them up.
			totalAmountFrom += amountFrom;
			totalAmountFrom = MathHelper.round(totalAmountFrom, 10); // Removes fp precision errors.
			totalAmountTo += amountTo;
			totalAmountTo = MathHelper.round(totalAmountTo, 10); // Removes fp precision errors.

			// Add the entry.
			this._entries.push({ categoryId, amountFrom: amountFrom, amountTo: amountTo });
		}

		// Update the total amount.
		const totalAmountFromElem = form.getEntries().query('.totalAmountFrom', HTMLElement);
		totalAmountFromElem.innerHTML = displayAmount(totalAmountFrom, this._unitFrom, false);
		if (totalAmountFrom !== this._transaction.amountFrom) {
			totalAmountFromElem.style.color = 'red';
		}
		else {
			totalAmountFromElem.style.removeProperty('color');
		}
		const totalAmountToElem = form.getEntries().query('.totalAmountTo', HTMLElement);
		totalAmountToElem.innerHTML = displayAmount(totalAmountTo, this._unitTo, false);
		if (totalAmountTo !== this._transaction.amountTo) {
			totalAmountToElem.style.color = 'red';
		}
		else {
			totalAmountToElem.style.removeProperty('color');
		}
	}

	/** The form was canceled, so call the callback. */
	onFormCanceled(): void {
		this.app.closeOverlay(this);
	}

	protected async onFormSubmitted(_form: FormEasy): Promise<string> {

		// Check that a transaction was set.
		if (!this._transaction) {
			return '';
		}

		// Check that the totals equals the original totals.
		let totalAmountFrom = 0;
		for (const entry of this._entries) {
			totalAmountFrom += entry.amountFrom;
			totalAmountFrom = MathHelper.round(totalAmountFrom, 10); // Removes fp precision errors.
		}
		if (this._transaction.amountTo !== totalAmountFrom) {
			return 'The total amount from does not equal the transaction amount from.';
		}
		let totalAmountTo = 0;
		for (const entry of this._entries) {
			totalAmountTo += entry.amountFrom;
			totalAmountTo = MathHelper.round(totalAmountTo, 10); // Removes fp precision errors.
		}
		if (this._transaction.amountTo !== totalAmountTo) {
			return 'The total amount to does not equal the transaction amount to.';
		}

		try {
			// Do server commands.
			const newTransactionCommands: { dateFromOld: string; transaction: Transaction; }[] = [];
			for (const entry of this._entries) {
				// Create the new transaction.
				const newTransaction = { ...this._transaction };
				newTransaction.id = '';
				newTransaction.categoryId = entry.categoryId;
				newTransaction.amountFrom = entry.amountFrom;
				newTransaction.amountTo = entry.amountTo;
				newTransactionCommands.push({
					dateFromOld: newTransaction.dateFrom,
					transaction: newTransaction
				});
			}

			// Send the new to the server. Save the transaction id.
			const newTransactionIds = await (this.app as FinancioApp).send<string[]>('transactions', 'update', {
				projectId: (this.app as FinancioApp).projectId,
				transactions: newTransactionCommands
			});

			// Delete the this transaction.
			await this.app.send('transactions', 'delete', {
				projectId: this.app.projectId,
				id: this._transaction.id,
				dateFrom: this._transaction.dateFrom
			});

			// Get the list of new transactions with their new ids.
			const newTransactions = [];
			for (let i = 0; i < newTransactionIds.length; i++) {
				const newTransaction = newTransactionCommands[i]!.transaction;
				newTransaction.id = newTransactionIds[i]!;
				newTransactions.push(newTransaction);
			}

			// Close this popup.
			this.app.closeOverlay(this);

			// Call the created callback.
			if (this.onSubmitted) {
				this.onSubmitted(newTransactions);
			}
		}
		catch (error) {
			return (error as Error).message;
		}
		return '';
	}

	/** The transaction we're spliting. */
	private _transaction: Transaction | undefined;

	/** The unit from for displaying the total. */
	private _unitFrom: Unit | undefined;

	/** The unit to for displaying the total. */
	private _unitTo: Unit | undefined;

	/** The entries. */
	private _entries: { categoryId: string; amountFrom: number; amountTo: number; }[] = [];

	/** Whether or not the from and to must match. */
	private _amountFromToMatch: boolean = false;

	protected static override css = css;
}
