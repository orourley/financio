import { Params } from '@orourley/elm-app';
import { DeletePopup } from './delete-popup';

export class UnitConversionRateDeletePopup extends DeletePopup {

	// TODO: Make a DeleteLightPopup, and inherit from this. It will only say "Are you sure?"
	// Or maybe just inherit from the Confirm Popup?

	/** Constructor. */
	constructor(params: Params) {
		super('Unit Conversion Rate', params);
	}

	protected override async getName(): Promise<string> {

		return '';
	}

	protected override async delete(id: string): Promise<void> {

		// Convert the id to a unitId, otherUnitId, and date.
		const idTokens = id.split(',');
		const unitId = idTokens[0];
		const otherUnitId = idTokens[1];
		const date = idTokens[2];

		// Send the command.
		await this.app.send('units', 'deleteConversionRate', {
			projectId: this.app.projectId,
			unitId: unitId,
			otherUnitId: otherUnitId,
			date: date
		});
	}
}
