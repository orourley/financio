import { Params } from '@orourley/elm-app';
import { ItemChooser } from './item-chooser';

export class ReportChooser extends ItemChooser {

	/** Constructor. */
	constructor(params: Params) {
		super('Report', 'reports', params);
	}
}
