import { Params } from '@orourley/elm-app';
import { Items } from '@orourley/financio-types';
import { DeletePopup } from './delete-popup';

export class GroupDeletePopup extends DeletePopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Group', params);

		const module = params.attributes.get('module');
		if (module === undefined) {
			throw new Error('A module must be specified.');
		}
		this._module = module;
	}

	protected override async getName(id: string): Promise<string> {

		// Get the category.
		const group = await this.app.send<Items.Group>(this._module, 'getGroup', {
			projectId: this.app.projectId,
			id: id
		});

		// Return the name.
		return group.name;
	}

	protected override async delete(id: string): Promise<void> {

		// Send the command.
		await this.app.send(this._module, 'deleteItemOrGroup', {
			projectId: this.app.projectId,
			id: id
		});
	}

	/** The module for the server command. */
	private _module: string;
}
