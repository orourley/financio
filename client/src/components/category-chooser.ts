import { Params } from '@orourley/elm-app';
import { ItemChooser } from './item-chooser';

export class CategoryChooser extends ItemChooser {

	/** Constructor. */
	constructor(params: Params) {
		super('Category', 'categories', params);
	}
}
