import { Params } from '@orourley/elm-app';
import { Report } from '@orourley/financio-types';
import { DeletePopup } from './delete-popup';

export class ReportDeletePopup extends DeletePopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Report', params);
	}

	protected override async getName(id: string): Promise<string> {

		// Get the report.
		const report = await this.app.send<Report>('reports', 'getItem', {
			projectId: this.app.projectId,
			id: id
		});

		// Return the name.
		return report.name;
	}

	protected override async delete(id: string): Promise<void> {

		// Send the command.
		await this.app.send('reports', 'deleteItemOrGroup', {
			projectId: this.app.projectId,
			id: id
		});
	}
}
