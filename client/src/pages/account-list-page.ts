import { Params } from '@orourley/elm-app';
import { ListPage } from './list-page';

/** The account list page. */
export class AccountListPage extends ListPage {

	/** Constructor. */
	constructor(params: Params) {
		super('Account', 'accounts', params);
	}
}
