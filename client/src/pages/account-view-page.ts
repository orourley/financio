import { Router } from '@orourley/elm-app';
import { FinancioPage, TransactionList } from '../internal';
import { Account } from '@orourley/financio-types';

import html from './account-view-page.html';

/** The account view page. */
export class AccountViewPage extends FinancioPage {

	/** Process the query. */
	override async processQuery(_oldQuery: Router.Query | undefined, query: Router.Query): Promise<void> {

		// Get the account id.
		const accountId = query['id'];
		if (accountId === undefined) {
			this.setHtml('No account specified.', this.root);
			return;
		}
		this._accountId = accountId;

		// Get the filter from the query.
		const transactionList = this.queryComponent('.TransactionList', TransactionList);
		transactionList.setFilterFromQuery(query);

		// Update the display.
		await this.updateDisplay();
	}

	/** Updates the display. */
	private async updateDisplay(): Promise<void> {

		// Get the account.
		const account = await this.app.send<Account>('accounts', 'getItem', {
			projectId: this.app.projectId,
			id: this._accountId
		});

		// Set the subtitle.
		this.app.setSubtitle(`Account - ${account.name}`);

		// Update the links.
		this.query('.links .import', HTMLAnchorElement).href += `&projectId=${this.app.projectId}&id=${this._accountId}`;

		// Update the transaction list.
		const transactionList = this.queryComponent('.TransactionList', TransactionList);
		await transactionList.setForcedFilter({
			accountId1: this._accountId
		});
	}

	/** Shows the edit popup. */
	openEditPopup(): void {
		this.app.openOverlay(`<AccountEditPopup typeId="${this._accountId}" edit onSaved="onSaved"></AccountEditPopup>`, this);
	}

	/** Shows the delete popup. */
	openDeletePopup(): void {
		this.app.openOverlay(`<AccountDeletePopup typeId="${this._accountId}" onDeleted="onDeleted"></AccountDeletePopup>`, this);
	}

	/** Called when this has been saved. */
	onSaved(): void {

		// Update the display.
		this.updateDisplay().catch((error) => {
			this.setHtml(`<p>${(error as Error).message}</p>`, this.root);
		});
	}

	/** Called when this has been deleted. */
	onDeleted(): void {

		// Go to the list.
		this.app.setRouterQuery({
			projectId: this.app.projectId,
			page: 'accountList'
		});
	}

	/** The id of the account. */
	private _accountId!: string;

	protected static override html = html;
}
