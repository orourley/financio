import { Router } from '@orourley/elm-app';
import { FinancioPage, TransactionList } from '../internal';
import { Category } from '@orourley/financio-types';

import html from './category-view-page.html';

/** The category view page. */
export class CategoryViewPage extends FinancioPage {

	/** Process the query. */
	override async processQuery(_oldQuery: Router.Query | undefined, query: Router.Query): Promise<void> {

		// Get the category id.
		const categoryId = query['id'];
		if (categoryId === undefined) {
			this.setHtml('No category specified.', this.root);
			return;
		}
		this._categoryId = categoryId;

		// Get the filter from the query.
		const transactionList = this.queryComponent('.TransactionList', TransactionList);
		transactionList.setFilterFromQuery(query);

		// Update the display.
		await this.updateDisplay().catch((error) => {
			this.setHtml(`<p>${(error as Error).message}</p>`, this.root);
		});

	}

	/** Updates the display. */
	private async updateDisplay(): Promise<void> {

		// Get the category.
		const category = await this.app.send<Category>('categories', 'getItem', {
			projectId: this.app.projectId,
			id: this._categoryId
		});

		// Set the subtitle.
		this.app.setSubtitle(`Category - ${category.name}`);

		// Update the transaction list.
		await this.queryComponent('.TransactionList', TransactionList).setForcedFilter({
			categoryIds: [this._categoryId]
		});
	}

	/** Shows the edit popup. */
	openEditPopup(): void {
		this.app.openOverlay(`<CategoryEditPopup typeId="${this._categoryId}" edit onSaved="onSaved"></CategoryEditPopup>`, this);
	}

	/** Shows the delete popup. */
	openDeletePopup(): void {
		this.app.openOverlay(`<CategoryDeletePopup typeId="${this._categoryId}" onDeleted="onDeleted"></CategoryDeletePopup>`, this);
	}

	/** Called when this has been saved. */
	onSaved(): void {

		// Update the display.
		this.updateDisplay().catch((error) => {
			this.setHtml(`<p>${(error as Error).message}</p>`, this.root);
		});
	}

	/** Called when this has been deleted. */
	onDeleted(): void {

		// Go to the list.
		this.app.setRouterQuery({
			projectId: this.app.projectId,
			page: 'categoryList'
		});
	}

	/** The id of the category. */
	private _categoryId!: string;

	protected static override html = html;
}
