import { Params } from '@orourley/elm-app';
import { ListPage } from './list-page';

/** The project list page. */
export class ProjectListPage extends ListPage {

	/** The constructor. */
	constructor(params: Params) {
		super('Project', 'projects', params);
	}
}
