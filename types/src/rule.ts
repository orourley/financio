import { Items } from './items';
import { Transaction } from './transaction';

/** A rule for sorting transactions into categories. */
export type Rule = Items.Item & {

	/** The reviewed flag to match. */
	reviewedMatch: boolean | undefined;

	/** Description and notes match as a regex. */
	searchMatch: string | undefined;

	/** Amount match. */
	amountMatch: number | undefined;

	/** The category to apply, if any. */
	categoryId: string | undefined;

	/** The account from or to, to apply, if any. */
	accountId: string | undefined;

	/** The notes to append, if any. */
	notes: string | undefined;

	/** The reviewed flag to apply, if any. */
	reviewed: boolean | undefined;

	/** The account to apply it on, if any. If undefined, it will apply it on all accounts. */
	onlyOnAccountId: string | undefined;

	/** Whether or not it is enabled. */
	enabled: boolean;
};

export namespace Rule {

	/** Applies a rule to a transaction. This assumes the rule matches the match fields. */
	export function apply(rule: Rule, transaction: Transaction): boolean {

		// A flag to determine if the transaction changed.
		let changed: boolean = false;

		// Disabled, do nothing.
		if (!rule.enabled) {
			return changed;
		}

		// Not the right account, do nothing.
		if (rule.onlyOnAccountId !== undefined && transaction.accountIdFrom !== rule.onlyOnAccountId && transaction.accountIdTo !== rule.onlyOnAccountId) {
			return changed;
		}

		// Match the reviewed flag.
		if (rule.reviewedMatch !== undefined && transaction.reviewed !== rule.reviewedMatch) {
			return changed;
		}

		// Match the search.
		if (rule.searchMatch !== undefined && transaction.description.match(rule.searchMatch) === null && transaction.note.match(rule.searchMatch) === null) {
			return changed;
		}

		// Match the amount.
		if (rule.amountMatch !== undefined && transaction.amountFrom !== rule.amountMatch && transaction.amountTo !== rule.amountMatch) {
			return changed;
		}

		// Apply the account id.
		if (rule.accountId !== undefined) {
			if (transaction.accountIdFrom === '' && transaction.accountIdFrom !== rule.accountId) {
				transaction.accountIdFrom = rule.accountId;
				changed = true;
			}
			if (transaction.accountIdTo === '' && transaction.accountIdTo !== rule.accountId) {
				transaction.accountIdTo = rule.accountId;
				changed = true;
			}
		}

		// Apply the category id.
		if (rule.categoryId !== undefined && transaction.categoryId === '' && transaction.categoryId !== rule.categoryId) {
			transaction.categoryId = rule.categoryId;
			changed = true;
		}

		// Apply the notes.
		if (rule.notes !== undefined && rule.notes !== '') {
			transaction.note += ` ${rule.notes}`;
			changed = true;
		}

		// Apply the reviewed flag.
		if (rule.reviewed !== undefined && transaction.reviewed !== rule.reviewed) {
			transaction.reviewed = rule.reviewed;
			changed = true;
		}

		return changed;
	}
}

export type RuleGroup = Items.Group;
export type RuleFile = Items.File<Rule, RuleGroup>;
