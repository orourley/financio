import { Items } from './items';

/** Notes for types of reports.
 *
 * A table with
 *   columns of days, months, quarters, years, or nothing, and the total on the right.
 *   rows of selected categories.
 *   total of all columns as last row.
 *
 * A table with
 * 	single column with total.
 * 	rows of selected accounts.
 *  total of column as last row.
 */

/** A report. */
export type Report = Items.Item & {

	/** The start date as a RelativeDate string. */
	startDate: string;

	/** The end date as a RelativeDate string, inclusive. */
	endDate: string;

	/** The interval per period. */
	periodIntervals: 'days' | 'weeks' | 'months' | 'quarters' | 'years';

	/** The unit in which to do the report. If undefined, it uses the project default unit. */
	unitId: string | undefined;

} & ({

	/** The style. */
	style: 'incomeStatement';

	/** The list of categories to include.
	 *  If a top level is specified, all children are included.
	 *  Undefined means everything. */
	categoryIds: string[] | undefined;
} | {

	/** The style. */
	style: 'balanceSheet';

	/** The list of accounts to include.
	 *  If a top level is specified all children are included.
	 *  Undefined means everything. */
	accountIds: string[] | undefined;

	/** Whether to use the start or end of the day for reports. */
	useEndOfDay: boolean;
});

export type ReportGroup = Items.Group;
export type ReportFile = Items.File<Report, ReportGroup>;
