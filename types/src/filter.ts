import { JsonHelper, JsonType } from '@orourley/pine-lib';
import { RelativeDate } from './relative_date';

/** A filter for transactions. */
export type Filter = {

	/** The maximum number of transactions to return. */
	count?: number;

	/** Start listing transactions after this id. */
	afterId?: string;

	/** Start listing transactions at this date as a RelativeDate string. */
	startDate?: string;

	/** End listing transactions after this date as a RelativeDate string. */
	endDate?: string;

	/** Only list transactions with either an amountFrom or amountTo greater than or equal to this. */
	minAmount?: number;

	/** Only list transactions with either an amountFrom or amountTo less than or equal to this. */
	maxAmount?: number;

	/** Only list transactions with a description or notes that matches this regular expression. */
	search?: string;

	/** Only match transactions that are reviewed or unreviewed. */
	reviewed?: boolean;

	/*
	Options:
	account from or to
	account1 and account2, from or to
	any but account1 or account2
	*/

	/** Only match transactions with this accountId in the from or to direction.
	 *  Transactions matching this are intersected with transactions matching the other accountId.
	 *  They can also be groups and all children will be in the filter. */
	accountId1?: string;

	/** Only match transactions with this accountId in the from or to direction.
	 *  Transactions matching this are intersected with transactions matching the other accountId.
	 *  They can also be groups and all children will be in the filter. */
	accountId2?: string;

	/** If true, instead of including the accountId1 and accountId2, choose all transactions that
	 *  don't have either either accountId1 or accountId2 in their from or to direction. */
	excludeAccounts?: boolean;

	/** Only match transactions with these categoryIds. */
	categoryIds?: string[];
};

/** Returns true if the JSON is a filter. */
export function isFilter(json: JsonType | undefined): json is Filter {
	if (!JsonHelper.isObject(json)
		|| (!JsonHelper.isNumber(json['count']) && json['count'] !== undefined)
		|| (!JsonHelper.isString(json['afterId']) && json['afterId'] !== undefined)
		|| (!RelativeDate.isValidString(json['startDate']) && json['startDate'] !== undefined)
		|| (!RelativeDate.isValidString(json['endDate']) && json['endDate'] !== undefined)
		|| (!JsonHelper.isNumber(json['minAmount']) && json['minAmount'] !== undefined)
		|| (!JsonHelper.isNumber(json['maxAmount']) && json['maxAmount'] !== undefined)
		|| (!JsonHelper.isString(json['search']) && json['search'] !== undefined)
		|| (!JsonHelper.isBoolean(json['reviewed']) && json['reviewed'] !== undefined)
		|| (!JsonHelper.isString(json['accountId1']) && json['accountId1'] !== undefined)
		|| (!JsonHelper.isString(json['accountId2']) && json['accountId2'] !== undefined)
		|| (!JsonHelper.isBoolean(json['excludeAccounts']) && json['excludeAccounts'] !== undefined)
		|| (!JsonHelper.isArrayOfStrings(json['categoryIds']) && json['categoryIds'] !== undefined)) {
		return false;
	}
	return true;
}
