export module Items {

	/** An item stub for listing. */
	export type Item = {

		/** A unique id. */
		id: string;

		/** The name. */
		name: string;

		/** The description. */
		description?: string;

		/** The parent id. */
		parentId?: string;
	};

	/** A type that has children. */
	export type HasChildren = {

		/** The list of child ids. */
		childIds: string[];

		/** Are the children sorted? */
		sortChildren: boolean;
	};

	/** A group of items or more groups. */
	export type Group = Item & HasChildren;

	/** The format of the file. */
	export type File<ItemType extends Item = Item, GroupType extends Group = Group> = HasChildren & {

		/** The list of all items. */
		list: Record<string, ItemType | GroupType>;
	};

	export function isGroup<GroupType extends Group>(itemOrGroupOrFile: Item | GroupType | File | undefined): itemOrGroupOrFile is GroupType {
		return itemOrGroupOrFile !== undefined && 'childIds' in itemOrGroupOrFile && 'id' in itemOrGroupOrFile;
	}

	export function isFile<FileType extends File>(itemOrGroupOrFile: Item | Group | FileType | undefined): itemOrGroupOrFile is FileType {
		return itemOrGroupOrFile !== undefined && 'childIds' in itemOrGroupOrFile && 'list' in itemOrGroupOrFile;
	}

	export function hasChildren<GroupType extends Group, FileType extends File>(itemOrGroupOrFile: Item | Group | FileType | undefined): itemOrGroupOrFile is GroupType | FileType {
		return itemOrGroupOrFile !== undefined && 'childIds' in itemOrGroupOrFile;
	}

	/** Returns true if the item/group/file contains the other item/group/file, inclusively. */
	export function contains(itemOrGroupOrFile: Item | Group | File, possibleDescendent: Item | Group | File, file: File): boolean {

		// Return true if this is the possibleDescendant.
		if (itemOrGroupOrFile === possibleDescendent) {
			return true;
		}

		// If there's children, check them recursively.
		if (hasChildren(itemOrGroupOrFile)) {
			for (let i = 0; i < itemOrGroupOrFile.childIds.length; i++) {
				if (contains(file.list[itemOrGroupOrFile.childIds[i]!]!, possibleDescendent, file)) {
					return true;
				}
			}
		}

		// Didn't find it anywhere, return false.
		return false;
	}

	/** Iterates over all child items of the item, or the roots. If the intoChildren callback returns false, it will not go into those children. */
	export function iterateItems<ItemType extends Item, GroupType extends Group>(
		itemOrGroupOrFile: ItemType | GroupType | File<ItemType, GroupType>,
		itemFile: File<ItemType, GroupType>,
		itemCallback?: (itemOrGroupOrFile: ItemType | GroupType | File<ItemType, GroupType>, depth: number) => void,
		intoChildrenCallback?: (groupOrFile: GroupType | File<ItemType, GroupType>, depth: number) => boolean,
		outOfChildrenCallback?: (groupOrFile: GroupType | File<ItemType, GroupType>, depth: number) => void,
		depth: number = 0): void {

		// Do the root.
		if (itemCallback) {
			itemCallback(itemOrGroupOrFile, depth);
		}

		// If we're a group or item file,
		if (hasChildren(itemOrGroupOrFile)) {

			// Call the into children callback. If it returns false, don't go into the children.
			if (!intoChildrenCallback || intoChildrenCallback(itemOrGroupOrFile, depth)) {

				// Iterate through the accounts.
				for (let i = 0, l = itemOrGroupOrFile.childIds.length; i < l; i++) {

					// Get the child.
					const childId = itemOrGroupOrFile.childIds[i]!;
					const child = itemFile.list[childId];
					if (!child) {
						throw new Error(`ChildId ${childId} not found in file list when iterating. Possible data corruption.`);
					}

					// Iterate the child.
					iterateItems(child, itemFile, itemCallback, intoChildrenCallback, outOfChildrenCallback, depth + 1);
				}
			}

			// Call the account callback.
			if (outOfChildrenCallback) {
				outOfChildrenCallback(itemOrGroupOrFile, depth);
			}
		}
	}
}
