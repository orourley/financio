import { JsonHelper, JsonType } from '@orourley/pine-lib';
import { YMD } from './ymd';

/** A way of specifying a date relative to another date with many options.
 * The string form is 'absolute,offset,period,align'. */
export type RelativeDate = {

	/** Start with this date (undefined means today). */
	absolute?: string;

	/** Offset by this number of periods. Undefined means 0. */
	offset?: number;

	/** The period for the offset and align. Undefined means days in the toYMD() function. */
	period?: 'days' | 'weeks' | 'months' | 'quarters' | 'years';

	/** Align to the beginning or ending of that period. Undefined means no alignment. */
	align?: 'beginning' | 'ending';
};

export namespace RelativeDate {

	/** Computes a YMD date from the relative date. */
	export function toYMD(relativeDate: RelativeDate): YMD {
		const ymd = relativeDate.absolute !== undefined
			? new YMD(relativeDate.absolute)
			: new YMD();
		if (relativeDate.period === 'days' || relativeDate.period === undefined) {
			ymd.day += relativeDate.offset ?? 0;
		}
		else if (relativeDate.period === 'weeks') {
			ymd.day += (relativeDate.offset ?? 0) * 7;
			if (relativeDate.align === 'beginning') {
				ymd.day -= ymd.dayOfWeek - 1;
			}
			else if (relativeDate.align === 'ending') {
				ymd.day += 7 - ymd.dayOfWeek;
			}
		}
		else if (relativeDate.period === 'months') {
			ymd.month += (relativeDate.offset ?? 0);
			if (relativeDate.align === 'beginning') {
				ymd.day = 1;
			}
			else if (relativeDate.align === 'ending') {
				ymd.day = ymd.daysInMonth;
			}
		}
		else if (relativeDate.period === 'quarters') {
			ymd.month += 3 * (relativeDate.offset ?? 0);
			if (relativeDate.align === 'beginning') {
				ymd.day = 1;
				ymd.month -= (ymd.month - 1) % 3;
			}
			else if (relativeDate.align === 'ending') {
				ymd.day = 1;
				ymd.month += (12 - ymd.month) % 3;
				ymd.day = ymd.daysInMonth;
			}
		}
		else {
			ymd.year += relativeDate.offset ?? 0;
			if (relativeDate.align === 'beginning') {
				ymd.month = 1;
				ymd.day = 1;
			}
			else if (relativeDate.align === 'ending') {
				ymd.day = 1;
				ymd.month = 12;
				ymd.day = ymd.daysInMonth;
			}
		}
		return ymd;
	}

	/** Returns true if the string is a valid RelativeDate string. */
	export function isValidString(dateString: JsonType | undefined): dateString is string {
		if (!JsonHelper.isString(dateString)) {
			return false;
		}
		try {
			fromString(dateString);
			return true;
		}
		catch {
			return false;
		}
	}

	/** Converts a string into a RelativeDate. */
	export function fromString(dateString: string): RelativeDate {

		// Split the value into the 4 tokens: absolute, offset, period, align
		const tokens = dateString.split(',');
		if (tokens.length !== 4) {
			throw new Error(`dateString has an invalid value: ${dateString}`);
		}

		// Get the absolute part.
		const absolute = tokens[0] !== '' ? tokens[0] : undefined;

		// Get the offset part.
		const offset = tokens[1] !== '' ? parseInt(tokens[1]!) : undefined;
		if (offset !== undefined && isNaN(offset)) {
			throw new Error(`dateString has an invalid value for offset: "${tokens[1]}" in "${dateString}"`);
		}

		// Get the period part.
		const period = tokens[2] !== '' ? tokens[2] : undefined;
		if (period !== undefined && period !== 'days' && period !== 'weeks' && period !== 'months' && period !== 'quarters' && period !== 'years') {
			throw new Error(`dateString has an invalid value for period: "${tokens[2]}" in "${dateString}"`);
		}

		// Get the align part.
		const align = tokens[3] !== '' ? tokens[3] : undefined;
		if (align !== undefined && align !== 'beginning' && align !== 'ending') {
			throw new Error(`dateString has an invalid value for align: "${tokens[3]}" in "${dateString}"`);
		}

		return {
			...(absolute !== undefined && { absolute }),
			...(offset !== undefined && { offset }),
			...(period !== undefined && { period }),
			...(align !== undefined && { align })
		};
	}

	/** Converts a RelativeDate to a string. */
	export function toString(relativeDate: RelativeDate): string {
		return `${relativeDate.absolute ?? ''},${relativeDate.offset?.toFixed(0) ?? ''},${relativeDate.period ?? ''},${relativeDate.align ?? ''}`;
	}

	/** Returns true if the JSON is a relative date. */
	export function isValid(json: JsonType | undefined): json is RelativeDate {
		if (!JsonHelper.isObject(json)
			|| (!JsonHelper.isString(json['absolute']) && json['absolute'] !== undefined)
			|| (!JsonHelper.isNumber(json['offset']) && json['offset'] !== undefined)
			|| (json['period'] !== 'days' && json['period'] !== 'weeks' && json['period'] !== 'months' && json['period'] !== 'quarters' && json['period'] !== 'years' && json['period'] !== undefined)
			|| (json['align'] !== 'beginning' && json['align'] !== 'ending' && json['align'] !== undefined)) {
			return false;
		}
		return true;
	}
}
