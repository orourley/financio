import { JsonHelper, JsonObject, JsonType } from '@orourley/pine-lib';
import { RelativeDate, Report, ReportGroup, Role } from '@orourley/financio-types';
import { FinancioServer } from '../financio_server';
import { ItemsModule } from './items';

export class ReportsModule extends ItemsModule<Report, ReportGroup> {

	constructor(server: FinancioServer) {
		super('report', 'reports', true, server);
	}

	getMinimumPermission(type: 'list' | 'get' | 'set'): Role | undefined {
		if (type === 'list') {
			return Role.DashBoard;
		}
		else if (type === 'get') {
			return Role.Viewer;
		}
		else if (type === 'set') {
			return Role.Bookkeeper;
		}
		return Role.Owner;
	}

	async setItemProperties(report: Report, _newItem: boolean, params: JsonObject): Promise<void> {

		// Get the start date.
		const startDate = params['startDate'];
		if (!RelativeDate.isValidString(startDate)) {
			throw new Error(`params.startDate must be a RelativeDate string.`);
		}

		// Get the end date.
		const endDate = params['endDate'];
		if (!RelativeDate.isValidString(endDate)) {
			throw new Error(`params.endDate must be a RelativeDate string.`);
		}

		// Get the period intervals.
		const periodIntervals = params['periodIntervals'];
		if (periodIntervals !== 'days' && periodIntervals !== 'weeks' && periodIntervals !== 'months' && periodIntervals !== 'quarters' && periodIntervals !== 'years') {
			throw new Error(`params.periodIntervals must be either 'days', 'weeks', 'months', 'quarters', or 'years'.`);
		}

		// Get the unit id.
		const unitId = params['unitId'];
		if (!JsonHelper.isString(unitId) && unitId !== undefined) {
			throw new Error(`params.unitId must be a string or undefined.`);
		}

		// Get the category ids.
		const categoryIds = params['categoryIds'];
		if (!JsonHelper.isArrayOfStrings(categoryIds) && categoryIds !== undefined) {
			throw new Error(`params.categoryIds must be an array of strings or undefined.`);
		}

		// Get the account ids.
		const accountIds = params['accountIds'];
		if (!JsonHelper.isArrayOfStrings(accountIds) && accountIds !== undefined) {
			throw new Error(`params.accountIds must be an array of strings or undefined.`);
		}

		// Get the style.
		const style = params['style'];
		if (style !== 'incomeStatement' && style !== 'balanceSheet') {
			throw new Error(`params.style must be either 'incomeStatement' or 'balanceSheet'.`);
		}

		// Get the useEndOfDay.
		const useEndOfDay = params['useEndOfDay'];
		if (!JsonHelper.isBoolean(useEndOfDay)) {
			throw new Error(`params.useEndOfDay must be a boolean.`);
		}

		report.startDate = startDate;
		report.endDate = endDate;
		report.periodIntervals = periodIntervals;
		report.style = style;
		if (report.style === 'incomeStatement') {
			report.categoryIds = categoryIds;
		}
		else {
			report.accountIds = accountIds;
			report.useEndOfDay = useEndOfDay;
		}
		report.unitId = unitId;
	}

	onMessageReceivedExt(): Promise<JsonType | void> | undefined {
		return undefined;
	}
}
