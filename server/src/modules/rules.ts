import { JsonHelper, JsonObject, JsonType } from '@orourley/pine-lib';
import { Rule, Role, RuleGroup } from '@orourley/financio-types';
import { ItemsModule } from './items';
import { FinancioServer } from '../financio_server';

export class RulesModule extends ItemsModule<Rule, RuleGroup> {

	constructor(server: FinancioServer) {
		super('rule', 'rules', true, server);
	}

	/** Gets the permission given a request type. */
	getMinimumPermission(type: 'list' | 'get' | 'set'): Role | undefined {
		if (type === 'list') {
			return Role.Viewer;
		}
		else if (type === 'get') {
			return Role.Viewer;
		}
		else if (type === 'set') {
			return Role.Bookkeeper;
		}
		return Role.Owner;
	}

	/** Sets the attributes of a particular item. */
	async setItemProperties(rule: Rule, _newItem: boolean, params: JsonObject): Promise<void> {

		// Get the reviewed match.
		const reviewedMatch = params['reviewedMatch'];
		if (!JsonHelper.isBoolean(reviewedMatch) && reviewedMatch !== undefined) {
			throw new Error(`params.reviewedMatch must be a boolean or undefined.`);
		}

		// Get the search match.
		const searchMatch = params['searchMatch'];
		if (!JsonHelper.isString(searchMatch) && searchMatch !== undefined) {
			throw new Error(`params.searchMatch must be a string or undefined.`);
		}

		// Get the amount match.
		const amountMatch = params['amountMatch'];
		if (!JsonHelper.isNumber(amountMatch) && amountMatch !== undefined) {
			throw new Error(`params.amountMatch must be a number or undefined.`);
		}

		// Get the rule category id.
		const categoryId = params['categoryId'];
		if (!JsonHelper.isString(categoryId) && categoryId !== undefined) {
			throw new Error(`params.categoryId must be a string or undefined.`);
		}

		// Get the rule account id.
		const accountId = params['accountId'];
		if (!JsonHelper.isString(accountId) && accountId !== undefined) {
			throw new Error(`params.accountId must be a string or undefined.`);
		}

		// Get the rule notes.
		const notes = params['notes'];
		if (!JsonHelper.isString(notes) && notes !== undefined) {
			throw new Error(`params.notes must be a string or undefined.`);
		}

		// Get the rule reviewed flag.
		const reviewed = params['reviewed'];
		if (!JsonHelper.isBoolean(reviewed) && reviewed !== undefined) {
			throw new Error(`params.reviewed must be a boolean or undefined.`);
		}

		// Get the rule account id to apply it on.
		const onlyOnAccountId = params['onlyOnAccountId'];
		if (!JsonHelper.isString(onlyOnAccountId) && onlyOnAccountId !== undefined) {
			throw new Error(`params.onlyOnAccount must be a string or undefined.`);
		}

		// Get the rule enabled flag.
		const enabled = params['enabled'];
		if (!JsonHelper.isBoolean(enabled)) {
			throw new Error(`params.enabled must be a boolean.`);
		}

		rule.reviewedMatch = reviewedMatch;
		rule.searchMatch = searchMatch;
		rule.amountMatch = amountMatch;
		rule.categoryId = categoryId;
		rule.accountId = accountId;
		rule.notes = notes;
		rule.reviewed = reviewed;
		rule.onlyOnAccountId = onlyOnAccountId;
		rule.enabled = enabled;
	}

	onMessageReceivedExt(): Promise<JsonType | void> | undefined {
		return undefined;
	}
}
