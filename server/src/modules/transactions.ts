import { Connection, Module, RandomString } from '@orourley/ironbark-server';
import { JsonHelper, JsonObject, JsonType, MathHelper, Sort } from '@orourley/pine-lib';
import { Role, isFilter, RelativeDate, Transaction, isTransaction, YMD, AccountFile, CategoryFile, Items, UnitFile } from '@orourley/financio-types';
import { FinancioServer } from '../financio_server';

export class TransactionsModule extends Module<FinancioServer> {

	/** Process a message passed by the server. */
	onMessageReceived(command: string, params: JsonObject, connection: Connection): Promise<JsonType | void> {

		// Get the username.
		const username = this.server.users.getUsername(connection);
		if (username === undefined) {
			throw new Error(`User not logged in.`);
		}

		// Process the command.
		if (command === 'list') {
			return this.list(username, params);
		}
		else if (command === 'update') {
			return this.update(username, params);
		}
		else if (command === 'delete') {
			return this.delete(username, params);
		}
		else if (command === 'checkForDuplicates') {
			return this.checkForDuplicates(username, params);
		}

		// Invalid command.
		throw new Error(`Invalid command "${command}".`);
	}

	/** Returns the list of transactions. \
	 * Params: projectId, filter. \
	 * Returns: Transactions[] */
	async list(username: string, params: JsonObject): Promise<Transaction[]> {

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		const filter = params['filter'];
		if (!isFilter(filter)) {
			throw new Error(`params.filter is not a Filter`);
		}

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Viewer)) {
			throw new Error('You do not have the required role.');
		}

		// Get the accounts.
		const accountFile = await this.server.dataStore.getJson<AccountFile>(`projects/${projectId}/accounts.json`);

		// Get the categories.
		const categoryFile = await this.server.dataStore.getJson<CategoryFile>(`projects/${projectId}/categories.json`);

		// Setup the start and end dates.
		const startDate = RelativeDate.toYMD(RelativeDate.fromString(filter.startDate ?? ',-6,months,'));
		const startDateString = startDate.toString();
		const endDate = RelativeDate.toYMD(RelativeDate.fromString(filter.endDate ?? ',,,'));
		const endDateString = endDate.toString();

		// Get all of the accountIds1 to search from the filter accountId1.
		// Some of the ids might be groups, so we grab all descendents.
		const accountIds1 = new Set<string>();
		if (filter.accountId1 !== undefined && accountFile) {
			const accountOrGroup = accountFile.list[filter.accountId1];
			if (accountOrGroup) {
				Items.iterateItems(accountOrGroup, accountFile, (accountOrGroupOrFile) => {
					if (!Items.hasChildren(accountOrGroupOrFile)) {
						accountIds1.add(accountOrGroupOrFile.id);
					}
				});
			}
		}

		// Get all of the accountIds2 to search from the filter accountId2.
		// Some of the ids might be groups, so we grab all descendents.
		const accountIds2 = new Set<string>();
		if (filter.accountId2 !== undefined && accountFile) {
			const accountOrGroup = accountFile.list[filter.accountId2];
			if (accountOrGroup) {
				Items.iterateItems(accountOrGroup, accountFile, (accountOrGroupOrFile) => {
					if (!Items.hasChildren(accountOrGroupOrFile)) {
						accountIds2.add(accountOrGroupOrFile.id);
					}
				});
			}
		}

		// Get all of the categoryIds to search from the filter categoryIds.
		// Some of the ids might be groups, so we grab all descendents.
		const categoryIds = new Set();
		if (filter.categoryIds && categoryFile) {
			for (const categoryId of filter.categoryIds) {
				const categoryOrGroup = categoryFile.list[categoryId];
				if (!categoryOrGroup) {
					continue;
				}
				Items.iterateItems(categoryOrGroup, categoryFile, (categoryOrGroupOrFile) => {
					if (!Items.hasChildren((categoryOrGroupOrFile))) {
						categoryIds.add(categoryOrGroupOrFile.id);
					}
				});
			}
		}

		// Get the transactions based on a filter.
		const transactions: Transaction[] = [];
		const currentMonth = new YMD(endDate.year, endDate.month, 1);
		let numMonths = 0;
		while (true) {

			// Only search for so many months.
			numMonths += 1;
			if (numMonths > 120) {
				break;
			}

			// Stop if we've got enough transactions.
			if (transactions.length === filter.count) {
				break;
			}

			// Get all of the transactions for that month.
			const transactionsForMonth = await this.server.dataStore.getJson<Transaction[]>(`projects/${projectId}/transactions/${currentMonth.toYearMonthString()}.json`);
			if (transactionsForMonth) {

				// Find the index of the afterId transaction.
				let afterIdIndex = -1;
				if (filter.afterId !== undefined) {
					afterIdIndex = transactionsForMonth.findIndex((transaction) => transaction.id === filter.afterId);
				}

				// Extract only the transactions we want.
				for (let i = 0, l = transactionsForMonth.length; i < l; i++) {
					const transaction = transactionsForMonth[i]!;
					if (transactions.length === filter.count) {
						break;
					}
					if (i <= afterIdIndex) {
						continue;
					}
					if (transaction.dateFrom < startDateString && transaction.dateTo < startDateString) {
						continue;
					}
					if (transaction.dateFrom > endDateString && transaction.dateTo > endDateString) {
						continue;
					}
					if (filter.minAmount !== undefined && transaction.amountFrom < filter.minAmount && transaction.amountTo < filter.minAmount) {
						continue;
					}
					if (filter.maxAmount !== undefined && transaction.amountFrom > filter.maxAmount && transaction.amountTo > filter.maxAmount) {
						continue;
					}
					if (filter.search !== undefined) {
						const regex = new RegExp(filter.search, 'iu');
						if (transaction.description.match(regex) === null && transaction.note.match(regex) === null) {
							continue;
						}
					}
					if (filter.reviewed !== undefined && transaction.reviewed !== filter.reviewed) {
						continue;
					}
					if (filter.excludeAccounts === true) {
						if (filter.accountId1 !== undefined && (accountIds1.has(transaction.accountIdFrom) || accountIds1.has(transaction.accountIdTo))) {
							continue;
						}
						if (filter.accountId2 !== undefined && (accountIds2.has(transaction.accountIdFrom) || accountIds2.has(transaction.accountIdTo))) {
							continue;
						}
					}
					else {
						if (filter.accountId1 !== undefined && !accountIds1.has(transaction.accountIdFrom) && !accountIds1.has(transaction.accountIdTo)) {
							continue;
						}
						if (filter.accountId2 !== undefined && !accountIds2.has(transaction.accountIdFrom) && !accountIds2.has(transaction.accountIdTo)) {
							continue;
						}
					}
					if (filter.categoryIds && !categoryIds.has(transaction.categoryId)) {
						continue;
					}
					transactions.push(transaction);
				}
			}

			// Stop if we're past the startDate.
			if (currentMonth <= startDate) {
				break;
			}
			currentMonth.month -= 1;
		}

		// Return the transactions.
		return transactions;
	}

	/** Adds or edits a list of transactions. */
	async update(username: string, params: JsonObject): Promise<string[]> {

		type UpdateParamTransaction = { dateFromOld: string; transaction: Transaction; };

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the transaction.
		const transactions = params['transactions'];
		if (!JsonHelper.isArrayOfV<UpdateParamTransaction>(transactions, (entry): entry is UpdateParamTransaction => {
			if (!JsonHelper.isObject(entry)) {
				return false;
			}
			if (!JsonHelper.isString(entry['dateFromOld'])) {
				return false;
			}
			if (!isTransaction(entry['transaction'])) {
				return false;
			}
			return true;
		})) {
			throw new Error(`params.transactions must be of the format { dateFromOld: String, transaction: Transaction }`);
		}

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Bookkeeper)) {
			throw new Error('You do not have the required role.');
		}

		// Modify the account file.
		const ids: string[] = [];
		await this.server.dataStore.modifyJson<AccountFile>(`projects/${projectId}/accounts.json`, async (accountFile) => {

			// Check that it is valid.
			if (!accountFile) {
				throw new Error('Account file was not found');
			}

			// Go through each transaction we're updating. Record the ids.
			for (const entry of transactions) {
				const oldTransactionFileName = this.dateToFileName(entry.dateFromOld);
				const newTransactionFileName = this.dateToFileName(entry.transaction.dateFrom);
				const transaction = entry.transaction;

				// If we're just updating a transaction, remove the transaction from the old file.
				// It may or may not be a different file from the new file.
				if (transaction.id !== '') {

					// Load up the old file for modifying.
					await this.server.dataStore.modifyJson<Transaction[]>(`projects/${projectId}/transactions/${oldTransactionFileName}.json`, async (transactionsForMonth) => {

						// Check that it is valid.
						if (!transactionsForMonth) {
							throw new Error(`Transaction with id ${transaction.id} is not found.`);
						}

						// Get the old transaction from the list.
						const oldTransactionIndex = transactionsForMonth.findIndex((transactionToTest) => transactionToTest.id === transaction.id);
						if (oldTransactionIndex === -1) {
							throw new Error(`Transaction with id ${transaction.id} is not found.`);
						}
						const oldTransaction = transactionsForMonth[oldTransactionIndex]!;

						// Remove the transaction from the list.
						transactionsForMonth.splice(oldTransactionIndex, 1);

						// Go through the two accounts and update the valuesAtMonthStart.
						this.updateValuesAtMonthStart(oldTransaction.accountIdFrom, accountFile, oldTransaction.dateFrom, oldTransaction.amountFrom, false);
						this.updateValuesAtMonthStart(oldTransaction.accountIdTo, accountFile, oldTransaction.dateTo, -oldTransaction.amountTo, false);

						// Return the file.
						return transactionsForMonth;
					});
				}

				// Load up the new file for modifying. Right now, the transaction is nowhere.
				await this.server.dataStore.modifyJson<Transaction[]>(`projects/${projectId}/transactions/${newTransactionFileName}.json`, async (transactionsForMonth) => {

					if (!transactionsForMonth) {
						transactionsForMonth = [];
					}

					// Generate a new id if it doesn't yet have one.
					if (transaction.id === '') {
						transaction.id = RandomString.generate(16);
					}

					// Add the transaction into the list, sorted.
					Sort.add(transaction, transactionsForMonth, this.isTransactionLess);

					// Go through the two accounts and update the valuesAtMonthStart.
					this.updateValuesAtMonthStart(transaction.accountIdFrom, accountFile, transaction.dateFrom, -transaction.amountFrom, true);
					this.updateValuesAtMonthStart(transaction.accountIdTo, accountFile, transaction.dateTo, transaction.amountTo, true);

					// Return the file.
					return transactionsForMonth;
				});

				// If the transaction has accounts with different units, update the conversion tables.
				const accountFrom = accountFile.list[transaction.accountIdFrom];
				const accountTo = accountFile.list[transaction.accountIdTo];
				if (accountFrom && accountTo && !Items.isGroup(accountFrom) && !Items.isGroup(accountTo) && accountFrom.unitId !== accountTo.unitId) {
					await this.server.dataStore.modifyJson<UnitFile>(`projects/${projectId}/units.json`, async (unitFile) => {
						if (!unitFile) {
							throw new Error(`No units file found when updating transactions with accounts with units.`);
						}

						// Get the units from and to.
						const unitFrom = unitFile.list[accountFrom.unitId];
						if (!unitFrom || Items.isGroup(unitFrom)) {
							throw new Error(`Unit from not found.`);
						}
						const unitTo = unitFile.list[accountTo.unitId];
						if (!unitTo || Items.isGroup(unitTo)) {
							throw new Error(`Unit to not found.`);
						}

						// Get the conversion tables.
						let conversionRatesFrom = unitFrom.conversionRates[accountTo.unitId];
						if (!conversionRatesFrom) {
							conversionRatesFrom = [];
							unitFrom.conversionRates[accountTo.unitId] = conversionRatesFrom;
						}
						let conversionRatesTo = unitTo.conversionRates[accountFrom.unitId];
						if (!conversionRatesTo) {
							conversionRatesTo = [];
							unitTo.conversionRates[accountFrom.unitId] = conversionRatesTo;
						}

						// Add the dates.
						Sort.addOrOverwrite([transaction.dateFrom, transaction.amountTo / transaction.amountFrom], conversionRatesFrom, (lhs, rhs) => lhs[0] < rhs[0], (lhs, rhs) => lhs[0] === rhs[0]);
						Sort.addOrOverwrite([transaction.dateTo, transaction.amountTo / transaction.amountFrom], conversionRatesFrom, (lhs, rhs) => lhs[0] < rhs[0], (lhs, rhs) => lhs[0] === rhs[0]);
						Sort.addOrOverwrite([transaction.dateFrom, transaction.amountFrom / transaction.amountTo], conversionRatesTo, (lhs, rhs) => lhs[0] < rhs[0], (lhs, rhs) => lhs[0] === rhs[0]);
						Sort.addOrOverwrite([transaction.dateTo, transaction.amountFrom / transaction.amountTo], conversionRatesTo, (lhs, rhs) => lhs[0] < rhs[0], (lhs, rhs) => lhs[0] === rhs[0]);

						// Return the file.
						return unitFile;
					});
				}

				// Save the id for the return.
				ids.push(transaction.id);
			}

			// Return the accounts file.
			return accountFile;
		});

		return ids;
	}

	/** Deletes a transaction. */
	async delete(username: string, params: JsonObject): Promise<void> {

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Get the transaction file name.
		const dataFrom = params['dateFrom'];
		if (!JsonHelper.isString(dataFrom)) {
			throw new Error(`params.dateFrom must be a string.`);
		}

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Bookkeeper)) {
			throw new Error('You do not have the required role.');
		}

		// Modify the account file.
		await this.server.dataStore.modifyJson<AccountFile>(`projects/${projectId}/accounts.json`, async (accountFile) => {

			// Check that it is valid.
			if (!accountFile) {
				throw new Error('Account file was not found');
			}

			// Get the file name for the transaction date.
			const transactionFileName = this.dateToFileName(dataFrom);

			// Get all of the transactions for that month.
			await this.server.dataStore.modifyJson<Transaction[]>(`projects/${projectId}/transactions/${transactionFileName}.json`, async (transactionsForMonth) => {

				// Check that it is valid.
				if (!transactionsForMonth) {
					throw new Error(`Transaction with id ${id} is not found.`);
				}

				// Get the transaction from the list.
				const transactionIndex = transactionsForMonth.findIndex((transaction) => transaction.id === id);
				if (transactionIndex === -1) {
					throw new Error(`Transaction with id ${id} is not found.`);
				}
				const transaction = transactionsForMonth[transactionIndex]!;

				// Remove it.
				transactionsForMonth.splice(transactionIndex, 1);

				// Go through the two accounts and update the valuesAtMonthStart.
				this.updateValuesAtMonthStart(transaction.accountIdFrom, accountFile, transaction.dateFrom, transaction.amountFrom, false);
				this.updateValuesAtMonthStart(transaction.accountIdTo, accountFile, transaction.dateTo, -transaction.amountTo, false);

				// If there's no more transactions in the month, remove the file and valuesAtMonth entry.

				// Return the file.
				if (transactionsForMonth.length > 0) {
					return transactionsForMonth;
				}
				return undefined;
			});

			// Return the file.
			return accountFile;
		});
	}

	/** Returns the two arrays, of new and duplicate transactions for importing. */
	async checkForDuplicates(username: string, params: JsonObject): Promise<{ newTransactions: Transaction[]; duplicateTransactions: Transaction[]; }> {

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the account id.
		const accountId = params['accountId'];
		if (!JsonHelper.isString(accountId)) {
			throw new Error(`params.accountId must be a string.`);
		}

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Bookkeeper)) {
			throw new Error('You do not have the required role.');
		}

		const transactions = params['transactions'];
		if (!JsonHelper.isArrayOfV<Transaction>(transactions, (entry): entry is Transaction => isTransaction(entry))) {
			throw new Error(`params.transactions must be an array of transactions.`);
		}

		const newTransactions: Transaction[] = [];
		const duplicateTransactions: Transaction[] = [];

		// Go through each transaction we're updating.
		for (const transaction of transactions) {

			// Get the file name for the transaction date.
			const transactionFileName = this.dateToFileName(transaction.dateFrom);

			const transactionsForMonth = await this.server.dataStore.getJson<Transaction[]>(`projects/${projectId}/transactions/${transactionFileName}.json`);

			// If the month file is not valid, it's a new transaction.
			if (!transactionsForMonth) {
				newTransactions.push(transaction);
				continue;
			}

			// Get the transaction from the list.
			const transactionIndex = transactionsForMonth.findIndex((transactionForMonth) => {
				if (transaction.accountIdFrom === accountId) {
					if (transaction.institutionIdFrom !== '') {
						return transactionForMonth.institutionIdFrom.includes(transaction.institutionIdFrom);
					}
					return transactionForMonth.accountIdFrom === accountId && transactionForMonth.amountFrom === transaction.amountFrom && transactionForMonth.dateFrom === transaction.dateFrom;
				}
				else if (transaction.accountIdTo === accountId) {
					if (transaction.institutionIdTo !== '') {
						return transactionForMonth.institutionIdTo.includes(transaction.institutionIdTo);
					}
					return transactionForMonth.accountIdTo === accountId && transactionForMonth.amountTo === transaction.amountTo && transactionForMonth.dateTo === transaction.dateTo;
				}
				return false;
			});
			if (transactionIndex === -1) {
				newTransactions.push(transaction);
				continue;
			}

			// There was another transaction in this month that matched.
			duplicateTransactions.push(transaction);
		}

		return {
			newTransactions,
			duplicateTransactions
		};
	}

	/** Updates all of the valuesAtMonthStart for a given account given a date. */
	updateValuesAtMonthStart(accountId: string, accountsFile: AccountFile, date: string, offset: number, add: boolean): void {

		// Get the account.
		const account = accountsFile.list[accountId];
		if (!account || Items.isGroup(account)) {
			return;
		}

		// The date is after the anchor point, so we will adjust the date's next month's valueAtMonthStart, and forward.
		let yearMonthIndex: number;
		if (date >= account.anchorPoint.month) {

			// Get the index of the next month.
			const ymd = new YMD(date);
			ymd.day = 1;
			ymd.month += 1;
			const monthToUpdate = ymd.toYearMonthString();
			yearMonthIndex = Sort.getIndex(monthToUpdate, account.valuesAtMonthStart, (lhs, rhs) => lhs.month < rhs);

			// If we're adding and the entry doesn't exist, we add it in using the previous month's value.
			if (add && account.valuesAtMonthStart[yearMonthIndex]?.month !== monthToUpdate) {
				let value;
				if (account.valuesAtMonthStart[yearMonthIndex - 1] && account.valuesAtMonthStart[yearMonthIndex - 1]!.month > account.anchorPoint.month) {
					value = account.valuesAtMonthStart[yearMonthIndex - 1]!.value;
				}
				else {
					value = account.anchorPoint.value;
				}
				account.valuesAtMonthStart.splice(yearMonthIndex, 0, { month: monthToUpdate, value: value, count: 0 });
			}

			// Go through the rest of the entries and offset them.
			for (let i = yearMonthIndex, l = account.valuesAtMonthStart.length; i < l; i++) {
				account.valuesAtMonthStart[i]!.value = MathHelper.round(account.valuesAtMonthStart[i]!.value + offset, 10);
			}
		}
		// The date is before the anchor point, so we will adjust the date's month's valueAtMonthStart, and backward.
		else {

			// Get the index of the next month.
			const ymd = new YMD(date);
			const monthToUpdate = ymd.toYearMonthString();
			yearMonthIndex = Sort.getIndex(monthToUpdate, account.valuesAtMonthStart, (lhs, rhs) => lhs.month < rhs);

			// If the entry doesn't exist, we add it in using the previous month's value.
			if (account.valuesAtMonthStart[yearMonthIndex]?.month !== monthToUpdate) {
				let value;
				if (account.valuesAtMonthStart[yearMonthIndex] && account.valuesAtMonthStart[yearMonthIndex]!.month < account.anchorPoint.month) {
					value = account.valuesAtMonthStart[yearMonthIndex]!.value;
				}
				else {
					value = account.anchorPoint.value;
				}
				account.valuesAtMonthStart.splice(yearMonthIndex, 0, { month: monthToUpdate, value: value, count: 0 });
			}

			// Go through the rest of the entries and offset them.
			for (let i = yearMonthIndex; i >= 0; i--) {
				account.valuesAtMonthStart[i]!.value = MathHelper.round(account.valuesAtMonthStart[i]!.value - offset, 10);
			}
		}

		// Increment or decrement the count.
		if (add) {
			account.valuesAtMonthStart[yearMonthIndex]!.count += 1;
		}
		else {
			account.valuesAtMonthStart[yearMonthIndex]!.count -= 1;
			if (account.valuesAtMonthStart[yearMonthIndex]!.count === 0) {
				account.valuesAtMonthStart.splice(yearMonthIndex, 1);
			}
		}
	}

	/** Converts a date string for use by a filename. */
	dateToFileName(date: string): string {
		return new YMD(date).toYearMonthString();
	}

	/** Returns true if lhs is less than rhs, for sorting transactions. */
	isTransactionLess(lhs: Transaction, rhs: Transaction): boolean {
		if (lhs.dateFrom.localeCompare(rhs.dateFrom) < 0) {
			return true;
		}
		if (lhs.amountFrom < rhs.amountFrom) {
			return true;
		}
		return lhs.id < rhs.id;
	}
}
