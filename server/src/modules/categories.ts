import { JsonType } from '@orourley/pine-lib';
import { FinancioServer } from '../financio_server';
import { ItemsModule } from './items';
import { Category, CategoryGroup, Role } from '@orourley/financio-types';

export class CategoriesModule extends ItemsModule<Category, CategoryGroup> {

	constructor(server: FinancioServer) {
		super('category', 'categories', true, server);
	}

	getMinimumPermission(type: 'list' | 'get' | 'set'): Role | undefined {
		if (type === 'list') {
			return Role.DashBoard;
		}
		else if (type === 'get') {
			return Role.DashBoard;
		}
		else if (type === 'set') {
			return Role.Accountant;
		}
		return Role.Owner;
	}

	async setItemProperties(): Promise<void> {
	}

	onMessageReceivedExt(): Promise<JsonType | void> | undefined {
		return undefined;
	}
}
