import { Connection, Module, RandomString } from '@orourley/ironbark-server';
import { JsonHelper, JsonObject, JsonType, Sort } from '@orourley/pine-lib';
import { Role, Items } from '@orourley/financio-types';
import { FinancioServer } from '../financio_server';

export abstract class ItemsModule<ItemType extends Items.Item, GroupType extends Items.Group> extends Module<FinancioServer> {

	itemType: string;
	filename: string;
	needsProject: boolean;

	/** Gets the permission given a request type. */
	abstract getMinimumPermission(type: 'list' | 'get' | 'set'): Role | undefined;

	/** Sets the attributes of a particular item. */
	abstract setItemProperties(item: ItemType, newItem: boolean, params: JsonObject): Promise<void>;

	/** Runs any additional commands. Returns undefined if no command was processed. */
	abstract onMessageReceivedExt(command: string, username: string, params: JsonObject): Promise<JsonType | void> | undefined;

	constructor(itemType: string, filename: string, needsProject: boolean, server: FinancioServer) {
		super(server);

		this.itemType = itemType;
		this.filename = filename;
		this.needsProject = needsProject;
	}

	/** Process a message passed by the server. */
	onMessageReceived(command: string, params: JsonObject, connection: Connection): Promise<JsonType | void> {

		// Get the username.
		const username = this.server.users.getUsername(connection);
		if (username === undefined) {
			throw new Error(`User not logged in.`);
		}

		// Process the command.
		if (command === 'list') {
			return this.list(username, params);
		}
		else if (command === 'getItem') {
			return this.getItem(username, params);
		}
		else if (command === 'getGroup') {
			return this.getGroup(username, params);
		}
		else if (command === 'setItemOrGroup') {
			return this.setItemOrGroup(username, params);
		}
		else if (command === 'deleteItemOrGroup') {
			return this.deleteItemOrGroup(username, params);
		}
		else if (command === 'getFile') {
			return this.getFile(username, params);
		}
		else if (command === 'getFileSansList') {
			return this.getFileSansList(username, params);
		}
		else if (command === 'setFile') {
			return this.setFile(username, params);
		}
		else if (command === 'moveItemOrGroup') {
			return this.moveItemOrGroup(username, params);
		}

		// Try any extra command.
		const result = this.onMessageReceivedExt(command, username, params);
		if (result) {
			return result;
		}

		// Invalid command.
		throw new Error(`Invalid command "${command}".`);
	}

	/** Returns the file as a list with just the base Item and Group properties. \
	 * Params: projectId \
	 * Returns: Items.File<Items.Item, Items.Group> */
	async list(username: string, params: JsonObject): Promise<Items.File> {

		// Get the item filename and check the permissions.
		let itemFilename;
		if (this.needsProject) {

			// Get the project id.
			const projectId = params['projectId'];
			if (!JsonHelper.isString(projectId)) {
				throw new Error(`params.projectId must be a string.`);
			}

			// Verify permissions.
			if (!await this.server.hasPermission(username, projectId, this.getMinimumPermission('list'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `projects/${projectId}/${this.filename}.json`;
		}
		else {
			itemFilename = `${this.filename}.json`;
		}

		// Get the item file.
		const itemFile = await this.server.dataStore.getJson<Items.File>(itemFilename);
		if (!itemFile) {
			return {
				childIds: [],
				list: {},
				sortChildren: true
			};
		}

		// Convert the list into a simplified format with just the basics.
		const simplifiedList: Record<string, Items.Item | Items.Group> = {};
		for (const [id, item] of Object.entries(itemFile.list)) {
			if (Items.isGroup(item)) {
				simplifiedList[id] = {
					id: item.id,
					name: item.name,
					...(item.description !== undefined && { description: item.description }),
					...(item.parentId !== undefined && { parentId: item.parentId }),
					childIds: item.childIds,
					sortChildren: item.sortChildren
				};
			}
			else {
				simplifiedList[id] = {
					id: item.id,
					name: item.name,
					...(item.description !== undefined && { description: item.description }),
					...(item.parentId !== undefined && { parentId: item.parentId })
				};
			}
		}

		// Return the simplified file.
		return {
			childIds: itemFile.childIds,
			sortChildren: itemFile.sortChildren,
			list: simplifiedList
		};
	}

	/** Gets an item. \
	 * Params: projectId, id \
	 * Returns: ItemType */
	private async getItem(username: string, params: JsonObject): Promise<ItemType> {

		const item = await this.getItemOrGroup(username, params);
		if (!item || Items.isGroup(item)) {
			throw new Error(`The ${this.itemType} was not found.`);
		}

		return item;
	}

	/** Gets a group. \
	 * Params: projectId, id \
	 * Returns: GroupType */
	private async getGroup(username: string, params: JsonObject): Promise<GroupType> {

		const group = await this.getItemOrGroup(username, params);
		if (!group || !Items.isGroup(group)) {
			throw new Error(`The ${this.itemType} group was not found.`);
		}

		return group;
	}

	/** Gets an item or group. \
	 * Params: projectId, id \
	 * Returns: ItemType | GroupType */
	private async getItemOrGroup(username: string, params: JsonObject): Promise<ItemType | GroupType | undefined> {

		// Get the id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Get the item filename and check the permissions.
		let itemFilename;
		if (this.needsProject) {

			// Get the project id.
			const projectId = params['projectId'];
			if (!JsonHelper.isString(projectId)) {
				throw new Error(`params.projectId must be a string.`);
			}

			// Verify permissions.
			if (!await this.server.hasPermission(username, projectId, this.getMinimumPermission('get'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `projects/${projectId}/${this.filename}.json`;
		}
		else {

			// Verify permissions.
			if (!await this.server.hasPermission(username, id, this.getMinimumPermission('get'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `${this.filename}.json`;
		}

		// Get the item file.
		const itemFile = await this.server.dataStore.getJson<Items.File<ItemType, GroupType>>(itemFilename);
		if (!itemFile) {
			return undefined;
		}

		// Return the item or group given its id.
		return itemFile.list[id];
	}

	/** Adds or edits an item. Returns the id. \
	 * Params: projectId, id, name, description, parentId, sortChildren (if group), item-specific params (if item). \
	 * Returns: id of item or group */
	async setItemOrGroup(username: string, params: JsonObject): Promise<string> {

		// Get the id.
		const id = params['id'];
		if (!JsonHelper.isString(id) && id !== undefined) {
			throw new Error(`params.id must be a string or undefined.`);
		}

		// Get the item filename and check the permissions.
		let itemFilename;
		if (this.needsProject) {

			// Get the project id.
			const projectId = params['projectId'];
			if (!JsonHelper.isString(projectId)) {
				throw new Error(`params.projectId must be a string.`);
			}

			// Verify permissions.
			if (!await this.server.hasPermission(username, projectId, this.getMinimumPermission('set'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `projects/${projectId}/${this.filename}.json`;
		}
		else {

			// Verify permissions if we're just editing an existing project.
			if (id !== undefined && !await this.server.hasPermission(username, id, this.getMinimumPermission('set'))) {
				throw new Error(`You do not have the required role.`);
			}

			// If we're adding a project, verify we're an admin.
			if (id === undefined && !await this.server.users.isAdmin(username)) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `${this.filename}.json`;
		}

		// Get the name.
		const name = params['name'];
		if (!JsonHelper.isString(name)) {
			throw new Error(`params.name must be a string.`);
		}

		// Get the description.
		const description = params['description'];
		if (!JsonHelper.isString(description) && description !== undefined) {
			throw new Error(`params.description must be a string | undefined.`);
		}

		// Update the item file.
		let idToReturn = id ?? '';
		await this.server.dataStore.modifyJson<Items.File<ItemType, GroupType>>(itemFilename, async (itemFile) => {

			// If the file doesn't yet exist, create it.
			if (!itemFile) {
				itemFile = {
					childIds: [],
					list: {},
					sortChildren: true
				};
			}

			// We're just updating an item.
			if (id !== undefined) {

				// Get the item or group given its id.
				const itemOrGroupQuery = itemFile.list[id];
				if (!itemOrGroupQuery) {
					throw new Error(`The ${this.itemType} or group was not found.`);
				}
				const itemOrGroup = itemOrGroupQuery;

				// Update the name. If sortChild of its parent is true, we need to resort.
				if (name !== itemOrGroup.name) {
					itemOrGroup.name = name;
					const parent = itemOrGroup.parentId !== undefined ? itemFile.list[itemOrGroup.parentId] : itemFile;
					if (!Items.hasChildren(parent)) {
						throw new Error(`The parent of the ${this.itemType} or group doesn't exist! Possible data corruption.`);
					}
					if (parent.sortChildren) {
						this.sortChildren(parent, itemFile);
					}
				}

				// Update the description.
				if (description !== itemOrGroup.description) {
					if (description !== undefined) {
						itemOrGroup.description = description;
					}
					else {
						delete itemOrGroup.description;
					}
				}

				// Update item or group specific properties.
				if (Items.isGroup(itemOrGroup)) {

					// Get the sortChildren.
					const sortChildren = params['sortChildren'];
					if (!JsonHelper.isBoolean(sortChildren)) {
						throw new Error(`params.sortChildren must be a boolean.`);
					}

					// Sort children if it will now be turned on.
					if (sortChildren && !itemOrGroup.sortChildren) {
						this.sortChildren(itemOrGroup, itemFile);
					}

					// Set the property.
					itemOrGroup.sortChildren = sortChildren;
				}
				else {

					// Set any custom item properties.
					await this.setItemProperties(itemOrGroup, false, params);
				}
			}
			// It's a new item or group.
			else {

				// Get the parentId.
				const parentId = params['parentId'];
				if (!JsonHelper.isString(parentId) && parentId !== undefined) {
					throw new Error(`params.parentId must be a string or undefined.`);
				}

				// Get the parent.
				const parent = parentId !== undefined ? itemFile.list[parentId] : itemFile;
				if (!Items.hasChildren(parent)) {
					throw new Error(`params.parentId is not a valid parent.`);
				}

				// Get the sortChildren.
				const sortChildren = params['sortChildren'];
				if (!JsonHelper.isBoolean(sortChildren) && sortChildren !== undefined) {
					throw new Error(`params.sortChildren must be a boolean or undefined.`);
				}

				// Get a new id.
				do {
					idToReturn = RandomString.generate(16);
				} while (itemFile.list[idToReturn] !== undefined);

				// Create a new item and add it to the list.
				const itemOrGroup = {
					id: idToReturn,
					name: name,
					description: description,
					...(parentId !== undefined && { parentId }),
					...(sortChildren !== undefined && { sortChildren: sortChildren, childIds: [] })
				} as ItemType | GroupType;

				// Set any custom item properties.
				if (!Items.isGroup(itemOrGroup)) {
					await this.setItemProperties(itemOrGroup, true, params);
				}

				// Add it to the main list.
				itemFile.list[itemOrGroup.id] = itemOrGroup;

				// Add it to the parent's children.
				parent.childIds.push(itemOrGroup.id);
				if (parent.sortChildren) {
					this.sortChildren(parent, itemFile);
				}
			}

			// Return the item file.
			return itemFile;
		});

		// Return the id that existed or was created.
		return idToReturn;
	}

	/** Deletes an item. \
	 * Params: projectId, id */
	async deleteItemOrGroup(username: string, params: JsonObject): Promise<void> {

		// Get the id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Get the item filename and check the permissions.
		let itemFilename;
		if (this.needsProject) {

			// Get the project id.
			const projectId = params['projectId'];
			if (!JsonHelper.isString(projectId)) {
				throw new Error(`params.projectId must be a string.`);
			}

			// Verify permissions.
			if (!await this.server.hasPermission(username, projectId, this.getMinimumPermission('set'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `projects/${projectId}/${this.filename}.json`;
		}
		else {

			// Verify permissions.
			if (!await this.server.hasPermission(username, id, this.getMinimumPermission('set'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `${this.filename}.json`;
		}

		// Update the item file.
		await this.server.dataStore.modifyJson<Items.File>(itemFilename, async (itemFile) => {

			// If undefined, do nothing.
			if (!itemFile) {
				throw new Error(`The ${this.itemType} was not found.`);
			}

			// Get the item or group given its id.
			const itemOrGroupQuery = itemFile.list[id];
			if (!itemOrGroupQuery) {
				throw new Error(`The ${this.itemType} was not found.`);
			}
			const itemOrGroup = itemOrGroupQuery;

			// If it's a group, verify that it's empty.
			if (Items.isGroup(itemOrGroup) && itemOrGroup.childIds.length > 0) {
				throw new Error(`The group must be empty.`);
			}

			// Get the parent of the item.
			const parent = itemOrGroup.parentId !== undefined ? itemFile.list[itemOrGroup.parentId] : itemFile;
			if (!Items.hasChildren(parent)) {
				throw new Error(`The item's parent doesn't exist! Possible data corruption.`);
			}

			// Remove it.
			const childIndex = parent.childIds.findIndex((childId) => childId === id);
			if (childIndex === -1) {
				throw new Error(`The item's parent doesn't have the item as a child! Possible data corruption.`);
			}
			parent.childIds.splice(childIndex, 1);
			delete itemFile.list[id];

			// Return the item file.
			return itemFile;
		});
	}

	/** Returns the whole file. \
	 * Params: projectId \
	 * Returns: Items.File<ItemType, GroupType> | undefined */
	async getFile(username: string, params: JsonObject): Promise<Items.File<ItemType, GroupType> | undefined> {

		// Get the item filename and check the permissions.
		let itemFilename;
		if (this.needsProject) {

			// Get the project id.
			const projectId = params['projectId'];
			if (!JsonHelper.isString(projectId)) {
				throw new Error(`params.projectId must be a string.`);
			}

			// Verify permissions.
			if (!await this.server.hasPermission(username, projectId, this.getMinimumPermission('get'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `projects/${projectId}/${this.filename}.json`;
		}
		else {
			itemFilename = `${this.filename}.json`;
		}

		// Get the item file.
		const itemFile = await this.server.dataStore.getJson<Items.File<ItemType, GroupType>>(itemFilename);

		// Return the file.
		return itemFile;
	}

	/** Gets the file without the list, for less bandwidth. \
	 * Params: projectId \
	 * Returns: ItemFile without 'list' */
	async getFileSansList(username: string, params: JsonObject): Promise<Omit<Items.File<ItemType, GroupType>, 'list'> | undefined> {

		// Get the item filename and check the permissions.
		let itemFilename;
		if (this.needsProject) {

			// Get the project id.
			const projectId = params['projectId'];
			if (!JsonHelper.isString(projectId)) {
				throw new Error(`params.projectId must be a string.`);
			}

			// Verify permissions.
			if (!await this.server.hasPermission(username, projectId, this.getMinimumPermission('get'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `projects/${projectId}/${this.filename}.json`;
		}
		else {

			// Verify permissions.
			if (!await this.server.users.isAdmin(username)) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `${this.filename}.json`;
		}

		// Get the item file.
		const itemFile = await this.server.dataStore.getJson<Items.File<ItemType, GroupType>>(itemFilename);
		if (!itemFile) {
			return undefined;
		}

		// Return the item file without the list property.
		return {
			childIds: itemFile.childIds,
			sortChildren: itemFile.sortChildren
		};
	}

	/** Edits an item file. \
	 * Params: projectId, sortChildren */
	async setFile(username: string, params: JsonObject): Promise<void> {

		// Get the item filename and check the permissions.
		let itemFilename;
		if (this.needsProject) {

			// Get the project id.
			const projectId = params['projectId'];
			if (!JsonHelper.isString(projectId)) {
				throw new Error(`params.projectId must be a string.`);
			}

			// Verify permissions.
			if (!await this.server.hasPermission(username, projectId, this.getMinimumPermission('set'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `projects/${projectId}/${this.filename}.json`;
		}
		else {

			// Verify permissions.
			if (!await this.server.users.isAdmin(username)) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `${this.filename}.json`;
		}

		// Get the sorting.
		const sortChildren = params['sortChildren'];
		if (!JsonHelper.isBoolean(sortChildren)) {
			throw new Error(`params.sortChildren must be a boolean.`);
		}

		// Update the item file.
		await this.server.dataStore.modifyJson<Items.File<ItemType, GroupType>>(itemFilename, async (itemFile) => {

			// If the file doesn't yet exist, create it.
			if (!itemFile) {
				itemFile = {
					childIds: [],
					list: {},
					sortChildren: true
				};
			}

			// Sort children if it will now be turned on.
			if (sortChildren && !itemFile.sortChildren) {
				this.sortChildren(itemFile, itemFile);
			}

			// Set the property.
			itemFile.sortChildren = sortChildren;

			// Return the item file.
			return itemFile;
		});
	}

	/** Moves an item or group. */
	async moveItemOrGroup(username: string, params: JsonObject): Promise<void> {

		// Get the item filename and check the permissions.
		let itemFilename;
		if (this.needsProject) {

			// Get the project id.
			const projectId = params['projectId'];
			if (!JsonHelper.isString(projectId)) {
				throw new Error(`params.projectId must be a string.`);
			}

			// Verify permissions.
			if (!await this.server.hasPermission(username, projectId, this.getMinimumPermission('set'))) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `projects/${projectId}/${this.filename}.json`;
		}
		else {

			// Verify permissions.
			if (!await this.server.users.isAdmin(username)) {
				throw new Error(`You do not have the required role.`);
			}

			itemFilename = `${this.filename}.json`;
		}

		// Get the id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Get the parent id.
		const parentId = params['parentId'];
		if (!JsonHelper.isString(parentId) && parentId !== undefined) {
			throw new Error(`params.parentId must be a string or undefined.`);
		}

		// Get the before id.
		const beforeId = params['beforeId'];
		if (!JsonHelper.isString(beforeId) && beforeId !== undefined) {
			throw new Error(`params.beforeId must be a string or undefined.`);
		}

		// Get the item file.
		await this.server.dataStore.modifyJson<Items.File>(itemFilename, async (itemFile) => {

			// If the item file doesn't yet exist, error.
			if (!itemFile) {
				throw new Error(`The ${this.itemType} or group was not found.`);
			}

			// Get the item or group given its id.
			const itemOrGroupQuery = itemFile.list[id];
			if (!itemOrGroupQuery) {
				throw new Error(`The ${this.itemType} or group was not found.`);
			}
			const itemOrGroup = itemOrGroupQuery;

			// Get the old parent of the item.
			const oldParent = itemOrGroup.parentId !== undefined ? itemFile.list[itemOrGroup.parentId] : itemFile;
			if (!Items.hasChildren(oldParent)) {
				throw new Error(`The item's old parent doesn't exist! Possible data corruption.`);
			}

			// Remove it from the old parent.
			const index = oldParent.childIds.findIndex((childId) => childId === id);
			if (index === -1) {
				throw new Error(`The item's old parent doesn't have the item as a child! Possible data corruption.`);
			}
			oldParent.childIds.splice(index, 1);

			// Get the parent.
			const newParent = parentId !== undefined ? itemFile.list[parentId] : itemFile;
			if (!Items.hasChildren(newParent)) {
				throw new Error(`params.parentId is not a valid parent.`);
			}

			// Add it to the parent's children.
			if (beforeId !== undefined) {
				const beforeIndex = newParent.childIds.findIndex((childId) => childId === beforeId);
				if (beforeIndex === -1) {
					throw new Error(`The ${this.itemType} to insert before doesn't exist on the new parent.`);
				}
				newParent.childIds.splice(beforeIndex, 0, itemOrGroup.id);
			}
			else {
				newParent.childIds.push(itemOrGroup.id);
			}
			if (newParent.sortChildren) {
				this.sortChildren(newParent, itemFile);
			}

			// Set the parentId.
			if (Items.isGroup(newParent)) {
				itemOrGroup.parentId = newParent.id;
			}
			else {
				delete itemOrGroup.parentId;
			}

			// Return the item file.
			return itemFile;
		});
	}

	/** Sorts the children of a group or file. */
	private sortChildren(groupOrItemFile: Items.Group | Items.File, itemFile: Items.File): void {
		Sort.sort(groupOrItemFile.childIds, (lhsId, rhsId) => {
			const lhs = itemFile.list[lhsId];
			const rhs = itemFile.list[rhsId];
			if (!lhs || !rhs) {
				throw new Error(`One or more of the item's parent's children don't exist! Possible data corruption.`);
			}
			return lhs.name.localeCompare(rhs.name) < 0;
		});
	}
}
