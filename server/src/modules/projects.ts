import { RandomString } from '@orourley/ironbark-server';
import { JsonHelper, JsonObject, JsonType } from '@orourley/pine-lib';
import { Project, ProjectGroup, Role } from '@orourley/financio-types';
import { ItemsModule } from './items';
import { FinancioServer } from '../financio_server';

export class ProjectsModule extends ItemsModule<Project, ProjectGroup> {

	constructor(server: FinancioServer) {
		super('project', 'projects', false, server);
	}

	getMinimumPermission(type: 'list' | 'get' | 'set'): Role | undefined {
		if (type === 'list') {
			return undefined;
		}
		else if (type === 'get') {
			return Role.DashBoard;
		}
		else if (type === 'set') {
			return Role.Owner;
		}
		return Role.Owner;
	}

	async setItemProperties(project: Project, newItem: boolean, params: JsonObject): Promise<void> {

		// Get the roles.
		const roles = params['roles'];
		if (!JsonHelper.isObjectOfV(roles, (role): role is Role => JsonHelper.isNumber(role) && role >= Role.DashBoard && role <= Role.Owner) && roles !== undefined) {
			throw new Error(`params.roles must be an object of numbers from ${Role.DashBoard} to ${Role.Owner} or undefined.`);
		}

		// Set the roles.
		if (roles) {
			project.roles = roles;
		}

		// If it's a new project, we need to set the default unit.
		if (newItem) {

			// Get the default unit name.
			const defaultUnitName = params['defaultUnitName'];
			if (!JsonHelper.isString(defaultUnitName)) {
				throw new Error('params.defaultUnitName must be a string.');
			}

			// Generate a default unit id.
			const defaultUnitId = RandomString.generate(16);

			// Create a new unit with the id and name.
			await this.server.dataStore.setJson(`projects/${project.id}/units.json`, {
				childIds: [
					defaultUnitId
				],
				sortChildren: true,
				list: {
					[defaultUnitId]: {
						id: defaultUnitId,
						name: defaultUnitName,
						conversionRates: {}
					}
				}
			});

			// Set the default unit id.
			project.defaultUnitId = defaultUnitId;
		}
		else {

			// Get the default unit id.
			const defaultUnitId = params['defaultUnitId'];
			if (!JsonHelper.isString(defaultUnitId) && defaultUnitId !== undefined) {
				throw new Error('params.defaultUnitId must be a string or undefined.');
			}

			if (defaultUnitId !== undefined) {
				project.defaultUnitId = defaultUnitId;
			}
		}
	}

	/** Deletes an item. \
	 * Params: id */
	override async deleteItemOrGroup(username: string, params: JsonObject): Promise<void> {

		// First run the super.
		// If it went through okay, we can safely delete the project folder.
		super.deleteItemOrGroup(username, params);

		// Get the id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Delete the project folder.
		await this.server.dataStore.delete(`projects/${id}`);
	}

	onMessageReceivedExt(): Promise<JsonType | void> | undefined {
		return undefined;
	}
}
