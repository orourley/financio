import { ServerJson as IronbarkServer } from '@orourley/ironbark-server';

import { AccountsModule } from './modules/accounts';
import { CategoriesModule } from './modules/categories';
import { ProjectsModule } from './modules/projects';
import { ReportsModule } from './modules/reports';
import { RulesModule } from './modules/rules';
import { TransactionsModule } from './modules/transactions';
import { UnitsModule } from './modules/units';

import { Items, ProjectFile, Role } from '../../types/src';

export class FinancioServer extends IronbarkServer {

	protected override async initialize(): Promise<void> {
		await super.initialize();

		// Make it so any user can view the list of all users.
		this.users.setEnablAnyoneListAllUsers(true);

		await this.registerModule('accounts', new AccountsModule(this));
		await this.registerModule('categories', new CategoriesModule(this));
		await this.registerModule('projects', new ProjectsModule(this));
		await this.registerModule('reports', new ReportsModule(this));
		await this.registerModule('rules', new RulesModule(this));
		await this.registerModule('transactions', new TransactionsModule(this));
		await this.registerModule('units', new UnitsModule(this));
	}

	/** Returns true if the user has the required role in the project or is an admin. */
	async hasPermission(username: string, projectId: string, minimumRole: Role | undefined): Promise<boolean> {

		// If there's no minimum role, anyone has permission.
		if (minimumRole === undefined) {
			return true;
		}

		// Get the project file.
		const projectFile = await this.dataStore.getJson<ProjectFile>('projects.json');
		if (!projectFile) {
			return false;
		}

		// Get the project.
		const project = projectFile.list[projectId];
		if (!project || Items.isGroup(project)) {
			return await this.users.isAdmin(username);
		}

		// Verify that the user has the required role in the project or is an admin.
		const userRole = project.roles[username];
		return (userRole !== undefined && userRole >= minimumRole) || await this.users.isAdmin(username);
	}
}

void FinancioServer.start();
